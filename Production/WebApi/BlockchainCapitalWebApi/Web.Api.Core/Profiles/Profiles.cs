﻿using AutoMapper;

namespace Web.Api.Core.Profiles
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<Infrastructure.Models.Address, Core.Models.Address>()
                .ForMember(dest => dest.IsExchangeAddress, opt => opt.Ignore())
                .ForMember(dest => dest.UserWalletIDs, opt=>opt.Ignore())
                .ForMember(dest => dest.IsSeedAddress, opt => opt.Ignore());
        }
    }
}
