﻿using NBitcoin;
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Services;

namespace Web.Api.Core.Interfaces
{
    public interface IBlockchainService
    {
        ServiceResult<string[]> GenerateMnemo();
        ServiceResult<GenerateAddressResponse> GenerateAddress(string mnemo, int keyNumber, Network net);
        ServiceResult<GenerateMultisigAddressResponse> GenerateMultisigAddress(List<string> privateKeys, int minSigsRequired, Network net);
        ServiceResult<string> GetPrivateKey(string mnemo, int keyNumber, Network net);
        ServiceResult<BalanceCheckResult> Balance(string address, bool isUnspentOnly, Network net, bool allowCached = false);
        ServiceResult<SendResult> Send(string privateKey, decimal btcVal, string toAddress, decimal minerFee, Network net);
        ServiceResult<SendResult> Send(List<string> privateKeys, decimal btcVal, string toAddress, decimal minerFee, int minSigsRequired, Network net);
    }
}
