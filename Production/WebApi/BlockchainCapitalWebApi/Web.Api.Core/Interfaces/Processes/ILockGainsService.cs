﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces.Processes
{
    public interface ILockGainsService
    {
        ServiceResult<UserProcess> Init(int userProcessId);
    }
}
