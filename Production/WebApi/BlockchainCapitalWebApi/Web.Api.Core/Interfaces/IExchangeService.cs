﻿using Web.Api.Core.Models;
using static Web.Api.Core.Services.ExchangeService;

namespace Web.Api.Core.Interfaces
{
    public interface IExchangeService
    {
        ServiceResult<decimal> Balance(int userId, ExchangeType type);
        ServiceResult<Address> GetAddress(int userId);
        ServiceResult<Transaction> Sell(int userId, decimal amountBTC);
        ServiceResult<Transaction> Buy(int userId, decimal amountFiat);
    }
}
