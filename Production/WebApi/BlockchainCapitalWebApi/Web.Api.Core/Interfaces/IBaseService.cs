﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Models;

namespace Web.Api.Core.Interfaces
{
    public interface IBaseService<T> where T : BaseEntity
    {
        //Task<IEnumerable<T>> GetAll();
        ServiceResult<List<T>> GetAll();
        ServiceResult<T> Get(int id);
    }
}
