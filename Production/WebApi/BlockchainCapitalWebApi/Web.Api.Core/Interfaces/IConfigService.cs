﻿
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IConfigService
    {
        NBitcoin.Network Network { get; }
        int ExchangeUserID { get; }

        ServiceResult<decimal> ExchangeRate(string curr);
    }
}
