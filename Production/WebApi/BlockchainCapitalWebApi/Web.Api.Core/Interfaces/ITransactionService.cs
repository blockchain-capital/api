﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface ITransactionService : IBaseService<Transaction>
    {
        ServiceResult<List<Transaction>> GetAll(Expression<Func<Infrastructure.Models.Transaction, bool>> exp);
        ServiceResult<List<Transaction>> GetAll(int addressId);
        ServiceResult<Transaction> Create(TransactionCreateRequest request);
        ServiceResult<Transaction> Update(TransactionUpdateRequest request);
    }
}
