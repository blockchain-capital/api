﻿
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IFeeTypeService : IBaseService<FeeType>
    {
        ServiceResult<FeeType> Create(FeeTypeCreateRequest request);
        ServiceResult<FeeType> GetByCode(string code);
    }
}
