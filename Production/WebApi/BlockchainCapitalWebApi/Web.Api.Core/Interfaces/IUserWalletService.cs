﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IUserWalletService : IBaseService<UserWallet>
    {
        ServiceResult<List<UserWallet>> GetAll(int userId);
        ServiceResult<List<UserWallet>> GetByAddressID(int addressId, Services.UserWalletService.AddressOwner owner = Services.UserWalletService.AddressOwner.Any);
        ServiceResult<UserWallet> Create(UserWalletCreateRequest request);
    }
}
