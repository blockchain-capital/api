﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IUserProcessService : IBaseService<UserProcess>
    {
        ServiceResult<List<UserProcess>> GetAll(int userId);
        ServiceResult<UserProcess> Create(UserProcessCreateRequest request);
        ServiceResult<UserProcess> Update(UserProcessUpdateRequest request);
    }
}
