﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using static Web.Api.Core.Services.AddressService;

namespace Web.Api.Core.Interfaces
{
    public interface IAddressService : IBaseService<Address>
    {
        ServiceResult<Address> Get(string address);
        ServiceResult<List<Address>> GetByUserWalletID(int userWalletId, Type type = Type.Any);
        ServiceResult<List<Address>> GetByUserID(int userId, Type type = Type.Any);
        ServiceResult<Address> Create(AddressCreateRequest request);
        ServiceResult<Address> Update(AddressUpdateRequest request);
        ServiceResult<Address> Balance(int addressId);
        ServiceResult<Address> Balance(string address);
    }
}
