﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces.Monitors
{
    public interface ITransactionMonitorService
    {
        ServiceResult<List<Transaction>> Monitor();
        ServiceResult<Transaction> Monitor(int trxId);
    }
}
