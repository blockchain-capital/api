﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces.Monitors
{
    public interface IUserProcessMonitorService
    {
        ServiceResult<List<UserProcess>> Monitor();
        ServiceResult<UserProcess> Monitor(int userProcessId);
    }
}
