﻿
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IOptionService : IBaseService<Option>
    {
        ServiceResult<Option> GetByCode(string code);
        ServiceResult<Option> Create(OptionCreateRequest request);
    }
}
