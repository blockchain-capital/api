﻿
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IUserService : IBaseService<User>
    {
        ServiceResult<User> GetByUserWalletID(int userWalletId);
        ServiceResult<User> GetByAddressID(int addressId);
        ServiceResult<User> Create(UserCreateRequest request);
        ServiceResult<User> Login(UserLoginRequest request);
        ServiceResult<string> PrivateKey(int addressId);
    }
}
