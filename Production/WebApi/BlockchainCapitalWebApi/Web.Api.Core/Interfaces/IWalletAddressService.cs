﻿
using System.Collections.Generic;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Interfaces
{
    public interface IWalletAddressService : IBaseService<WalletAddress>
    {
        ServiceResult<List<WalletAddress>> GetByUserID(int userId);
        ServiceResult<List<WalletAddress>> GetByUserWalletID(int userWalletId);
        ServiceResult<List<WalletAddress>> GetByAddressID(int addressId);
        ServiceResult<WalletAddress> Create(WalletAddressCreateRequest request);
    }
}
