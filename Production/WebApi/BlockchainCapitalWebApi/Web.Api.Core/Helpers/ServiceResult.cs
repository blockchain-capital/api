﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Cryptography;
using System;
using System.Text;

namespace Web.Api.Core.Models
{
    public class ServiceResult<T>
    {
        public T Object { get; set; }
        public bool Success { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
