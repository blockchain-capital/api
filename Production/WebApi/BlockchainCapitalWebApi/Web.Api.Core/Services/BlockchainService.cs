﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;
using NBitcoin;
using QBitNinja.Client;
using QBitNinja.Client.Models;

namespace Web.Api.Core.Services
{
    public class BlockchainService : IBlockchainService
    {
        private static int _confirmationsNeeded = 0;

        public BlockchainService()
        {
        }

        public ServiceResult<string[]> GenerateMnemo()
        {
            Mnemonic mnemonic = new Mnemonic(Wordlist.English, WordCount.Twelve);
            string mnemo = mnemonic.ToString();

            if (string.IsNullOrEmpty(mnemo))
                return new ServiceResult<string[]> { Success = false, Code = -1, Message = "Failed to generate mnemonic" };

            return new ServiceResult<string[]> { Success = true, Code = 1, Object = mnemonic.Words };
        }
        public ServiceResult<GenerateAddressResponse> GenerateAddress(string mnemo, int keyNumber, Network net)
        {
            Mnemonic restoreNnemo = new Mnemonic(mnemo);
            ExtKey masterKey = restoreNnemo.DeriveExtKey();
            //KeyPath keypth = new KeyPath("m /44'/0'/0'/0/" + ssKeynumber.ToString());
            KeyPath keypth = new KeyPath("44'/0'/0'/0/" + keyNumber);
            ExtKey key = masterKey.Derive(keypth);
            string address = key.PrivateKey.PubKey.GetAddress(net).ToString();
            string privateKey = key.PrivateKey.GetBitcoinSecret(net).ToString();

            if (string.IsNullOrEmpty(address) || string.IsNullOrEmpty(privateKey))
                return new ServiceResult<GenerateAddressResponse> { Success = false, Code = -1, Message = "Failed to generate address and/or privatekey" };

            return new ServiceResult<GenerateAddressResponse> { Success = true, Code = 1, Object = new GenerateAddressResponse { Address = address, PrivateKey = privateKey } };
        }
        public ServiceResult<string> GetPrivateKey(string mnemo, int keyNumber, Network net)
        {
            Mnemonic restoreNnemo = new Mnemonic(mnemo);
            ExtKey masterKey = restoreNnemo.DeriveExtKey();
            //KeyPath keypth = new KeyPath("m /44'/0'/0'/0/" + ssKeynumber.ToString());
            KeyPath keypth = new KeyPath("44'/0'/0'/0/" + keyNumber);
            ExtKey key = masterKey.Derive(keypth);
            string address = key.PrivateKey.PubKey.GetAddress(net).ToString();
            string privateKey = key.PrivateKey.GetBitcoinSecret(net).ToString();

            return new ServiceResult<string> { Success = true, Code = 1, Object = privateKey };
        }
        public ServiceResult<GenerateMultisigAddressResponse> GenerateMultisigAddress(List<string> privateKeys, int minSigsRequired, Network net)
        {
            //var transaction = Transaction.Create(net);
            List<BitcoinSecret> secrets = new List<BitcoinSecret>();
            List<PubKey> pubKeys = new List<PubKey>();

            foreach (var pk in privateKeys)
            {
                var secret = new BitcoinSecret(pk);

                pubKeys.Add(secret.PubKey);
            }

            Script redeem = PayToMultiSigTemplate.Instance.GenerateScriptPubKey(minSigsRequired, pubKeys.ToArray());
            var address = redeem.Hash.GetAddress(net);

            return new ServiceResult<GenerateMultisigAddressResponse> { Success = true, Code = 1, Object = new GenerateMultisigAddressResponse { Address = address.ToString(), RedeemScript = redeem } };
        }
        public ServiceResult<BalanceCheckResult> Balance(string address, bool isUnspentOnly, Network net, bool allowCached = false)
        {
            var ssBalance = 0.0M;
            var ssConfirmedBalance = 0.0M;

            var trxConfirms = 0;
            var trxConfirmsNeeded = 0;

            //var addr = _addressService.Get(address).Result;
            //if (allowCached && (addr.Checked.HasValue && addr.Checked.Value >= DateTime.Now.AddMinutes(-5)))
            //{
            //    ssBalance = addr.Balance;
            //    ssConfirmedBalance = addr.BalanceConfirmed;
            //}
            //else
            {
                BalanceCheckQBitNinja(address, isUnspentOnly, net, out ssBalance, out ssConfirmedBalance, out trxConfirms, out trxConfirmsNeeded);
                //BalanceCheckBlockCypher(address, isUnspentOnly, net, out ssBalance, out ssConfirmedBalance, out trxConfirms, out trxConfirmsNeeded); //TODO: WebException: The remote server returned an error: (429) Too Many Requests.

                //Update the address with balances
                //addr.Checked = DateTime.Now;
                //addr.Balance = ssBalance;
                //addr.BalanceConfirmed = ssConfirmedBalance;
                //var r = _addressService.Update(addr).Result;
            }

            return new ServiceResult<BalanceCheckResult>
            {
                Success = true,
                Code = 1,
                Object = new BalanceCheckResult
                {
                    Balance = ssBalance,
                    BalanceConfirmed = ssConfirmedBalance,
                    Confirms = trxConfirms,
                    ConfirmsNeeded = trxConfirmsNeeded
                }
            };
        }
        public ServiceResult<SendResult> Send(string privateKey, decimal btcVal, string toAddress, decimal minerFee, Network net)
        {
            var ssTransactionId = "";
            var error = false;
            var errorMsg = "Sent";

            var transaction = NBitcoin.Transaction.Create(net);
            var bitcoinPrivateKey = new BitcoinSecret(privateKey);
            var fromAddress = bitcoinPrivateKey.GetAddress().ToString();

            decimal checkBalance, checkBalanceConfirmed = 0.00M;
            int checkConfirmations, checkConfirmdationsNeeded = 0;

            var balance = this.BalanceCheckQBitNinja(fromAddress, true, net, out checkBalance, out checkBalanceConfirmed, out checkConfirmations, out checkConfirmdationsNeeded);

            //if (balanceResult.BalanceConfirmed <= btcVal)
            if (checkBalanceConfirmed <= btcVal)
                return new ServiceResult<SendResult> { Success = false, Code = -1, Message = "The address doesn't have enough funds!" };

            QBitNinjaClient client = new QBitNinjaClient(net);
            //var balance = client.GetBalance(new BitcoinPubKeyAddress(fromAddress), true).Result;
            //var balance = client.GetBalance(new BalanceSelector(fromAddress), true).Result;
            //var blockr = new QBitNinjaTransactionRepository();

            //Add trx in
            //Get all transactions in for that address
            int txsIn = 0;
            if (balance.Operations.Count > 0)
            {
                var unspentCoins = new List<Coin>();
                foreach (var operation in balance.Operations)
                {
                    //string transaction = operation.TransactionId.ToString();

                    foreach (Coin receivedCoin in operation.ReceivedCoins)
                    {
                        OutPoint outpointToSpend = receivedCoin.Outpoint;
                        transaction.Inputs.Add(new TxIn() { PrevOut = outpointToSpend });
                        transaction.Inputs[txsIn].ScriptSig = bitcoinPrivateKey.ScriptPubKey;
                        txsIn = txsIn + 1;
                    }
                }
            }
            //add address to send money
            //var toPubKeyAddress = new BitcoinPubKeyAddress(toAddress, net);
            var toPubKeyAddress = BitcoinAddress.Create(toAddress, net);
            TxOut toAddressTxOut = new TxOut()
            {
                Value = new Money((decimal)btcVal, MoneyUnit.BTC),
                ScriptPubKey = toPubKeyAddress.ScriptPubKey
            };
            transaction.Outputs.Add(toAddressTxOut);
            //add address to send change
            decimal change = checkBalanceConfirmed - btcVal - minerFee;
            if (change > 0)
            {
                //var fromPubKeyAddress = new BitcoinPubKeyAddress(fromAddress);
                var fromPubKeyAddress = BitcoinAddress.Create(fromAddress, net);
                TxOut changeAddressTxOut = new TxOut()
                {
                    Value = new Money((decimal)change, MoneyUnit.BTC),
                    ScriptPubKey = fromPubKeyAddress.ScriptPubKey
                };
                transaction.Outputs.Add(changeAddressTxOut);
            }
            // sign transaction
            transaction.Sign(bitcoinPrivateKey, false);
            //Send transaction
            BroadcastResponse broadcastResponse = client.Broadcast(transaction).Result;
            //if (!broadcastResponse.Success || broadcastResponse.Error.ErrorCode > 0)
            if (!broadcastResponse.Success)
                return new ServiceResult<SendResult> { Success = false,  Code = -1, Message = "Error broadcasting transaction " + broadcastResponse.Error.ErrorCode + " : " + broadcastResponse.Error.Reason };

            ssTransactionId = transaction.GetHash().ToString();

            return new ServiceResult<SendResult>
            {
                Success = true,
                Code = 1,
                Message = errorMsg,
                Object = new SendResult
                {
                    TransactionID = ssTransactionId,
                    Date = DateTime.Now,
                    ResultMsg = errorMsg
                }
            };
        }
        public ServiceResult<SendResult> Send(List<string> privateKeys, decimal btcVal, string toAddress, decimal minerFee, int minSigsRequired, Network net)
        {
            var ssTransactionId = "";
            var errorMsg = "Sent";

            var ssIsUnspentOnly = false;
            
            var msAddress = this.GenerateMultisigAddress(privateKeys, minSigsRequired, net);

            //this.GetUnspentOutputs(msAddress, net);

            TransactionBuilder builderNew = Network.Main.CreateTransactionBuilder();
            List<TransactionBuilder> trxBuilders = new List<TransactionBuilder>();
            foreach (var pk in privateKeys)
                trxBuilders.Add(Network.TestNet.CreateTransactionBuilder());

            QBitNinjaClient client = new QBitNinjaClient(net);
            //var balance = client.GetBalance(new BitcoinPubKeyAddress(ssAddress), ssIsUnspentOnly).Result;
            var balance = client.GetBalance(new BalanceSelector(msAddress.Object.Address), ssIsUnspentOnly).Result;

            var ssBalance = 0.0M;
            var ssConfirmedBalance = 0.0M;

            var trxConfirms = 0;
            var trxConfirmsNeeded = 0;

            if (balance.Operations.Count > 0)
            {
                var unspentCoins = new List<Coin>();
                var spentCoins = new List<Coin>();
                var unspentCoinsConfirmed = new List<Coin>();
                var spentCoinsConfirmed = new List<Coin>();

                foreach (var operation in balance.Operations)
                {
                    unspentCoins.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
                    spentCoins.AddRange(operation.SpentCoins.Select(coin => coin as Coin));
                    if (operation.Confirmations >= _confirmationsNeeded)
                    {
                        unspentCoinsConfirmed.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
                        spentCoinsConfirmed.AddRange(operation.SpentCoins.Select(coin => coin as Coin));
                    }
                    else
                    {
                        trxConfirms += operation.Confirmations;
                        trxConfirmsNeeded += _confirmationsNeeded;
                    }
                }
                ssBalance = unspentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC)) - spentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC));
                ssConfirmedBalance = unspentCoinsConfirmed.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC)) - spentCoinsConfirmed.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC));

                List<ScriptCoin> redeemCoins = new List<ScriptCoin>();

                foreach (var uc in unspentCoins)
                {
                    var add = true;
                    foreach (var sc in spentCoins)
                    {
                        if (sc.Outpoint == uc.Outpoint)
                            add = false;
                    }
                    if (add)
                        redeemCoins.Add(uc.ToScriptCoin(msAddress.Object.RedeemScript));
                }

                if (ssConfirmedBalance < decimal.Round(btcVal, 8))
                {
                    errorMsg = string.Format("Amount is more than Balance - Balance={0}, Amount={1}", ssConfirmedBalance, btcVal);
                }
                else
                {
                    if (ssConfirmedBalance - minerFee > 0)
                    {
                        NBitcoin.Transaction unsignedNew = builderNew
                        //.AddCoins(unspentCoins)
                        .AddCoins(redeemCoins)
                        .Send(BitcoinAddress.Create(toAddress, net), Money.Coins(btcVal - minerFee))
                        .SendFees(Money.Coins(minerFee))
                        .SetChange(BitcoinAddress.Create(msAddress.Object.Address, net), ChangeType.Uncolored)
                        .BuildTransaction(sign: false);

                        List<NBitcoin.Transaction> trxs = new List<NBitcoin.Transaction>();
                        for (var i = 0; i < trxBuilders.Count; i++)
                        {
                            var trxBuilder = trxBuilders[i];
                            var pk = privateKeys.ElementAt(i);

                            trxs.Add(trxBuilder
                            .AddCoins(redeemCoins)
                            .SendFees(Money.Coins(minerFee))
                            .AddKeys(new BitcoinSecret(pk, net))
                            .SignTransaction(unsignedNew));
                        }

                        NBitcoin.Transaction fullySigned = builderNew
                            .AddCoins(redeemCoins)
                            .SendFees(Money.Coins(minerFee))
                            .CombineSignatures(trxs.ToArray());

                        //Send transaction
                        BroadcastResponse broadcastResponse = client.Broadcast(fullySigned).Result;
                        if (!broadcastResponse.Success)
                            return new ServiceResult<SendResult> { Success = false, Code = 1, Message = "Error broadcasting transaction " + broadcastResponse.Error.ErrorCode + " : " + broadcastResponse.Error.Reason };

                        ssTransactionId = fullySigned.GetHash().ToString();
                    }
                    else
                    {
                        return new ServiceResult<SendResult> { Success = false, Code = 1, Message = string.Format("Less than 0 - Balance={0}, MinerFee={1}", ssConfirmedBalance, minerFee) };
                    }
                }
            }
            else
            {
                return new ServiceResult<SendResult> { Success = false, Code = 1, Message = "No operations" };
            }

            return new ServiceResult<SendResult>
            {
                Success = true,
                Code = 1,
                Object = new SendResult
                {
                    TransactionID = ssTransactionId,
                    Date = DateTime.Now,
                    ResultMsg = errorMsg
                }
            };
        }

        private BalanceModel BalanceCheckQBitNinja(string address, bool isUnspentOnly, Network net, out decimal addressBalance, out decimal addressConfirmedBalance, out int confirms, out int confirmsNeeded)
        {
            var ssBalance = 0.0M;
            var ssConfirmedBalance = 0.0M;

            var trxConfirms = 0;
            var trxConfirmsNeeded = 0;

            QBitNinjaClient client = new QBitNinjaClient(net);
            //var balance = client.GetBalance(new BitcoinPubKeyAddress(ssAddress), ssIsUnspentOnly).Result;
            var balance = client.GetBalance(new BalanceSelector(address), isUnspentOnly).Result;

            if (balance.Operations.Count > 0)
            {
                var unspentCoins = new List<Coin>();
                var spentCoins = new List<Coin>();
                var unspentCoinsConfirmed = new List<Coin>();
                var spentCoinsConfirmed = new List<Coin>();

                foreach (var operation in balance.Operations)
                {
                    unspentCoins.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
                    spentCoins.AddRange(operation.SpentCoins.Select(coin => coin as Coin));
                    if (operation.Confirmations >= _confirmationsNeeded)
                    {
                        unspentCoinsConfirmed.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
                        spentCoinsConfirmed.AddRange(operation.SpentCoins.Select(coin => coin as Coin));
                    }
                    else
                    {
                        trxConfirms += operation.Confirmations;
                        trxConfirmsNeeded += _confirmationsNeeded;
                    }
                }
                ssBalance = unspentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC)) - spentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC));
                ssConfirmedBalance = unspentCoinsConfirmed.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC)) - spentCoinsConfirmed.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC));
            }

            if (ssConfirmedBalance < ssBalance)
            {
                addressConfirmedBalance = ssConfirmedBalance;
                addressBalance = ssBalance;
            }
            else
            {
                addressConfirmedBalance = ssBalance;
                addressBalance = ssConfirmedBalance;
            }

            //addressBalance = ssBalance;
            //addressConfirmedBalance = ssConfirmedBalance;

            confirms = trxConfirms;
            confirmsNeeded = trxConfirmsNeeded;

            return balance;
        }
    }

    public class GenerateAddressResponse
    {
        public string Address { get; set; }
        public string PrivateKey { get; set; }
    }
    public class GenerateMultisigAddressResponse
    {
        public string Address { get; set; }
        public Script RedeemScript { get; set; }
    }
    public class BalanceCheckResult
    {
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public int Confirms { get; set; }
        public int ConfirmsNeeded { get; set; }
    }
    public class SendResult
    {
        public string TransactionID { get; set; }
        public DateTime Date { get; set; }
        public string ResultMsg { get; set; }
    }
}
