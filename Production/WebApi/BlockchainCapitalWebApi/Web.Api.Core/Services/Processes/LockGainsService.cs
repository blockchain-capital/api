﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using AutoMapper;
using Web.Api.Core.Models.Dto;
using Web.Api.Core.Interfaces.Monitors;
using Web.Api.Core.Interfaces.Processes;
using System.Linq;

namespace Web.Api.Core.Services.Processes
{
    public class LockGainsService : ILockGainsService
    {
        private readonly IConfigService _configServ;
        private readonly IOptionService _optionServ;
        private readonly IFeeTypeService _feeTypeServ;
        private readonly IUserProcessService _userProcessServ;
        private readonly IUserService _userServ;
        private readonly ITransactionService _transactionServ;
        private readonly IAddressService _addressServ;
        private readonly IUserWalletService _userWalletServ;
        private readonly IExchangeService _exchangeServ;
        private readonly IMapper _mapper;

        private decimal mrtMinBalanceBTC = 0.00M;
        private decimal mrtCollateralBTC = 0.00M;
        private decimal mrtContributeBTC = 0.00M;

        public LockGainsService(IConfigService configServ, IOptionService optionServ, IFeeTypeService feeTypeServ, IUserProcessService userProcessServ, IUserService userServ, ITransactionService transactionServ, IAddressService addressServ, 
            IUserWalletService userWalletServ, IExchangeService exchangeServ, IMapper mapper)
        {
            _configServ = configServ;
            _optionServ = optionServ;
            _feeTypeServ = feeTypeServ;
            _userProcessServ = userProcessServ;
            _userServ = userServ;
            _transactionServ = transactionServ;
            _addressServ = addressServ;
            _userWalletServ = userWalletServ;
            _exchangeServ = exchangeServ;
            _mapper = mapper;
        }

        public ServiceResult<UserProcess> Init(int userProcessId)
        {
            var userProcess = _userProcessServ.Get(userProcessId);
            if (!userProcess.Success)
                return new ServiceResult<UserProcess> { Success = false, Code = userProcess.Code, Message = userProcess.Message };

            if (userProcess.Object.IsComplete)
                return new ServiceResult<UserProcess> { Success = true, Code = 2, Message = "User process already completed", Object = userProcess.Object };

            if (userProcess.Object.StatusID == 1)
            {
                if (userProcess.Object.ProgressIndex == 0)
                    userProcess.Object.ProgressIndex = 1;

                var result = this.Start(userProcess.Object);
                //Update user process with error
                var userProcessUpdate = new UserProcessUpdateRequest
                {
                    ID = userProcess.Object.ID,
                    StatusID = userProcess.Object.StatusID,
                    ProgressIndex = userProcess.Object.ProgressIndex,
                    ProgressMessage = result.Message,
                    HasError = !result.Success
                };
                if (result.Success)
                {
                    userProcessUpdate.StatusID = result.Object.StatusID;
                    userProcessUpdate.IsComplete = result.Object.IsComplete;
                    userProcessUpdate.ProgressIndex = result.Object.ProgressIndex;
                }

                var updateResult = _userProcessServ.Update(userProcessUpdate);
                if (!updateResult.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = updateResult.Code, Message = updateResult.Message };

                return new ServiceResult<UserProcess> { Success = true, Code = result.Code, Message = result.Message };
            }

            return new ServiceResult<UserProcess> { Success = true, Code = 2, Message = "No user processes found to run" };
        }
        
        private ServiceResult<UserProcess> Start(UserProcess userProcess)
        {
            //Get exchange rate
            var exchangeRate = _configServ.ExchangeRate("zar");
            if (!exchangeRate.Success)
                return new ServiceResult<UserProcess> { Success = false, Code = exchangeRate.Code, Message = exchangeRate.Message };

            var userFiat = userProcess.InitialAmount;
            var userBTC = userFiat / exchangeRate.Object;

            var lenderLeverage = _optionServ.GetByCode("Lf");
            var mrtFeeCollateral = _feeTypeServ.GetByCode("MRTc");
            var mrtFeeContribute = _feeTypeServ.GetByCode("MRTc2");
            var mrtFeeMin = _feeTypeServ.GetByCode("MRTm");

            //Calculate MRT min required balance - can be a percentage of the deposit amount and/or a fixed amount
            if (mrtFeeMin.Object.Percentage > 0)
                mrtMinBalanceBTC = userBTC * (mrtFeeMin.Object.Percentage / 100);
            if (mrtFeeMin.Object.Amount > 0)
                mrtMinBalanceBTC += mrtFeeMin.Object.Amount;

            //Calculate MRT collateral - can be a percentage of the deposit amount and/or a fixed amount
            if (mrtFeeCollateral.Object.Percentage > 0)
                mrtCollateralBTC = userBTC * (mrtFeeCollateral.Object.Percentage / 100);
            if (mrtFeeCollateral.Object.Amount > 0)
                mrtCollateralBTC += mrtFeeCollateral.Object.Amount;

            //Calculate MRT contribute - can be a percentage of the deposit amount and/or a fixed amount
            if (mrtFeeContribute.Object.Percentage > 0)
                mrtContributeBTC = userBTC * (mrtFeeContribute.Object.Percentage / 100);
            if (mrtFeeContribute.Object.Amount > 0)
                mrtContributeBTC += mrtFeeContribute.Object.Amount;

            //Calculate mrt collateral and contribute fiat amounts
            var mrtCollateralFiat = mrtCollateralBTC * exchangeRate.Object;

            var mrtContributeFiat = mrtContributeBTC * exchangeRate.Object;
            if (mrtContributeFiat > userFiat)
                mrtContributeFiat = userFiat;

            //Calculate BCC loan amount
            //TODO: What if MRT has enough funds, will BCC still provide loan - or do we leave it up to the fee parameters?
            var lenderFiat = 0.00M;
            var lenderBTC = 0.00M;
            if (mrtContributeFiat < userFiat)
            {
                lenderFiat = lenderLeverage.Object.ValueNumeric * (userFiat - mrtContributeFiat);
                lenderBTC = lenderFiat / exchangeRate.Object;
            }

            //USER PROCESS
            //1 Create user+MRT multisig
            if (userProcess.ProgressIndex == 1)
            {
                var result = this.UserMRTMultisig(userProcess);
                if (result.Success)
                    userProcess.ProgressIndex = 2;

                return new ServiceResult<UserProcess> { Success = result.Success, Code = result.Code, Message = result.Message, Object = userProcess };
            }

            //2 Check multisig balance, if enough 6, if not 3
            if (userProcess.ProgressIndex == 2)
            {
                var resultMS = this.UserMRTMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                var msg = "";
                if (result.Object.BalanceConfirmed >= userBTC)
                {
                    userProcess.ProgressIndex = 6;
                    msg = "Multisig has enough funds already";
                }
                else
                {
                    userProcess.ProgressIndex = 3;
                    msg = "Multisig needs funding";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //3 Check the user's exchange crypto balance, enough then 5, else 4
            if (userProcess.ProgressIndex == 3)
            {
                //Get the User's exchange account
                var user = _userServ.GetByAddressID(userProcess.UserAddressID);
                if (!user.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = user.Code, Message = user.Message };

                var balance = _exchangeServ.Balance(user.Object.ID, ExchangeService.ExchangeType.Crypto);
                if (!balance.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = balance.Code, Message = balance.Message };

                var msg = "";
                if (balance.Object >= userBTC)
                {
                    userProcess.ProgressIndex = 5;
                    msg = "User exchange crypto has enough funds";
                }
                else
                {
                    userProcess.ProgressIndex = 4;
                    msg = "User exchange crypto insufficient funds";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //4 Not enough crypto? Buy crypto with fiat then 3
            if (userProcess.ProgressIndex == 4)
            {
                //Get the User exchange account
                var user = _userServ.GetByAddressID(userProcess.UserAddressID);
                if (!user.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = user.Code, Message = user.Message };

                var buy = _exchangeServ.Buy(user.Object.ID, userFiat);
                if (!user.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = user.Code, Message = user.Message };

                userProcess.ProgressIndex = 3;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "User crypto bought with fiat", Object = userProcess };
            }

            //5 Send crypto from user's exchange address to multisig then 6
            if (userProcess.ProgressIndex == 5)
            {
                var resultMS = this.UserMRTMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                //Get the User's exchange account
                var user = _userServ.GetByAddressID(userProcess.UserAddressID);
                if (!user.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = user.Code, Message = user.Message };

                var userExchangeAddr = _exchangeServ.GetAddress(user.Object.ID);
                if (!userExchangeAddr.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = userExchangeAddr.Code, Message = userExchangeAddr.Message };

                var trx = _transactionServ.Create(new TransactionCreateRequest
                {
                    UserProcessID = userProcess.ID,
                    AmountBTC = userBTC,
                    Description = "User > USER & MRT Multisig",
                    SenderAddressID = userExchangeAddr.Object.ID,
                    RecipientAddressID = resultMS.Object.ID,
                    Type = TransactionType.NotSpecified
                });
                if (!trx.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = trx.Code, Message = trx.Message };

                userProcess.ProgressIndex = 6;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "User crypto sent to User & MRT multisig", Object = userProcess };
            }

            //6 Done - User+MRT multisig funded
            if (userProcess.ProgressIndex == 6)
            {
                var resultMS = this.UserMRTMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                if (result.Object.BalanceConfirmed >= userBTC)
                {
                    userProcess.ProgressIndex = 7;
                    return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Multisig funded", Object = userProcess };
                }

                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "Waiting for funds (TransactionMonitor running?)" };
            }

            //MRT PROCESS
            //7 Create MRT+BCC multisig
            if (userProcess.ProgressIndex == 7)
            {
                var result = this.MRTLenderMultisig(userProcess);
                if (result.Success)
                    userProcess.ProgressIndex = 8;

                return new ServiceResult<UserProcess> { Success = result.Success, Code = result.Code, Message = result.Message, Object = userProcess };
            }

            //8 Check multisig balance, if enough 12, if not 9
            if (userProcess.ProgressIndex == 8)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                var msg = "";
                if (result.Object.BalanceConfirmed >= mrtCollateralBTC + mrtContributeBTC)
                {
                    userProcess.ProgressIndex = 12;
                    msg = "Multisig has enough funds already";
                }
                else
                {
                    userProcess.ProgressIndex = 9;
                    msg = "Multisig needs funding";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //9 Check the MRT's exchange crypto balance, enough then 11, else 10
            if (userProcess.ProgressIndex == 9)
            {
                //Get the MRT exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var balance = _exchangeServ.Balance(mrt.Object.ID, ExchangeService.ExchangeType.Crypto);
                if (!balance.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = balance.Code, Message = balance.Message };

                var msg = "";
                if (balance.Object >= mrtContributeBTC + mrtCollateralBTC)
                {
                    userProcess.ProgressIndex = 11;
                    msg = "MRT exchange crypto has enough funds";
                }
                else
                {
                    userProcess.ProgressIndex = 10;
                    msg = "MRT exchange crypto insufficient funds";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //10 Not enough crypto? Buy crypto with fiat then 10
            if (userProcess.ProgressIndex == 10)
            {
                //Get the MRT exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var buy = _exchangeServ.Buy(mrt.Object.ID, userFiat);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                userProcess.ProgressIndex = 10;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "MRT crypto bought with fiat", Object = userProcess };
            }

            //11 Send crypto from MRT's exchange address to multisig then 12
            if (userProcess.ProgressIndex == 11)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                //Get the MRT's exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var mrtExchangeAddr = _exchangeServ.GetAddress(mrt.Object.ID);
                if (!mrtExchangeAddr.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrtExchangeAddr.Code, Message = mrtExchangeAddr.Message };

                var trx = _transactionServ.Create(new TransactionCreateRequest
                {
                    UserProcessID = userProcess.ID,
                    AmountBTC = mrtCollateralBTC + mrtContributeBTC,
                    Description = "MRT > MRT & LENDER Multisig",
                    SenderAddressID = mrtExchangeAddr.Object.ID,
                    RecipientAddressID = resultMS.Object.ID,
                    Type = TransactionType.NotSpecified
                });
                if (!trx.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = trx.Code, Message = trx.Message };

                userProcess.ProgressIndex = 12;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "MRT crypto sent to MRT & LENDER multisig", Object = userProcess };
            }

            //12 Done
            if (userProcess.ProgressIndex == 12)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                if (result.Object.BalanceConfirmed >= mrtCollateralBTC + mrtContributeBTC)
                {
                    userProcess.ProgressIndex = 13;
                    return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Multisig funded", Object = userProcess };
                }

                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "Waiting for funds (TransactionMonitor running?)" };
            }

            //LENDER PROCESS
            //13 Check multisig balance, if enough 17, if not 14
            if (userProcess.ProgressIndex == 13)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                var msg = "";
                if (result.Object.BalanceConfirmed >= (mrtCollateralBTC + mrtContributeBTC + lenderBTC))
                {
                    userProcess.ProgressIndex = 17;
                    msg = "Multisig has enough funds already";
                }
                else
                {
                    userProcess.ProgressIndex = 14;
                    msg = "Multisig needs funding";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //14 Check the LENDER's exchange crypto balance, enough then 16, else 15
            if (userProcess.ProgressIndex == 14)
            {
                //Get the MRT exchange account
                var lender = _userServ.GetByAddressID(userProcess.LenderAddressID);
                if (!lender.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = lender.Code, Message = lender.Message };

                var balance = _exchangeServ.Balance(lender.Object.ID, ExchangeService.ExchangeType.Crypto);
                if (!balance.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = balance.Code, Message = balance.Message };

                var msg = "";
                if (balance.Object >= lenderBTC)
                {
                    userProcess.ProgressIndex = 16;
                    msg = "LENDER exchange crypto has enough funds";
                }
                else
                {
                    userProcess.ProgressIndex = 15;
                    msg = "LENDER exchange crypto insufficient funds";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //15 Not enough crypto? Buy crypto with fiat then 14
            if (userProcess.ProgressIndex == 15)
            {
                //Get the LENDER exchange account
                var lender = _userServ.GetByAddressID(userProcess.LenderAddressID);
                if (!lender.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = lender.Code, Message = lender.Message };

                var buy = _exchangeServ.Buy(lender.Object.ID, userFiat);
                if (!lender.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = lender.Code, Message = lender.Message };

                userProcess.ProgressIndex = 14;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "LENDER crypto bought with fiat", Object = userProcess };
            }

            //16 Send crypto from LENDER's exchange address to multisig then 17
            if (userProcess.ProgressIndex == 16)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                //Get the LENDER's exchange account
                var lender = _userServ.GetByAddressID(userProcess.LenderAddressID);
                if (!lender.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = lender.Code, Message = lender.Message };

                var lenderExchangeAddr = _exchangeServ.GetAddress(lender.Object.ID);
                if (!lenderExchangeAddr.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = lenderExchangeAddr.Code, Message = lenderExchangeAddr.Message };

                var trx = _transactionServ.Create(new TransactionCreateRequest
                {
                    UserProcessID = userProcess.ID,
                    AmountBTC = lenderBTC,
                    Description = "LENDER > MRT & LENDER Multisig",
                    SenderAddressID = lenderExchangeAddr.Object.ID,
                    RecipientAddressID = resultMS.Object.ID,
                    Type = TransactionType.NotSpecified
                });
                if (!trx.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = trx.Code, Message = trx.Message };

                userProcess.ProgressIndex = 13;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "LENDER crypto sent to MRT & LENDER multisig", Object = userProcess };
            }

            //17 Done - MRT+LENDER Multisig funded
            if (userProcess.ProgressIndex == 17)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                var result = _addressServ.Balance(resultMS.Object.Addr);
                if (!result.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = result.Code, Message = result.Message };

                if (result.Object.BalanceConfirmed >= lenderBTC)
                {
                    userProcess.ProgressIndex = 18;
                    return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Multisig funded", Object = userProcess };
                }

                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "Waiting for funds (TransactionMonitor running?)" };
            }

            //MRT+LENDER PROCESS
            //18 - Send crypto (MRT contribute and LENDER amounts) from MRT+LENDER multisig to MRT exchange account then 19
            if (userProcess.ProgressIndex == 18)
            {
                var resultMS = this.MRTLenderMultisig(userProcess);
                if (!resultMS.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = resultMS.Code, Message = resultMS.Message };

                //Get the MRT+Lender address id
                var address = _addressServ.Get(resultMS.Object.Addr);
                if (!address.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = address.Code, Message = address.Message };

                //Get the MRT exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var mrtExchangeAddr = _exchangeServ.GetAddress(mrt.Object.ID);
                if (!mrtExchangeAddr.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrtExchangeAddr.Code, Message = mrtExchangeAddr.Message };

                var trx = _transactionServ.Create(new TransactionCreateRequest
                {
                    UserProcessID = userProcess.ID,
                    AmountBTC = mrtContributeBTC + lenderBTC,
                    Description = "MRT + Lender > MRT Exchange BTC",
                    SenderAddressID = address.Object.ID,
                    RecipientAddressID = mrtExchangeAddr.Object.ID,
                    Type = TransactionType.NotSpecified
                });
                if (!trx.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = trx.Code, Message = trx.Message };

                userProcess.ProgressIndex = 19;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Crypto sent to MRT exchange address", Object = userProcess };
            }

            //19 - Check MRT exchange crypto balance, if enough then 20
            if (userProcess.ProgressIndex == 19)
            {
                //Get the MRT exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var balance = _exchangeServ.Balance(mrt.Object.ID, ExchangeService.ExchangeType.Crypto);
                if (!balance.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = balance.Code, Message = balance.Message };

                var msg = "";
                if (balance.Object >= mrtContributeBTC + lenderBTC)
                {
                    userProcess.ProgressIndex = 20;
                    msg = "MRT exchange crypto has enough funds";
                }
                else
                {
                    msg = "MRT exchange crypto insufficient funds";
                }

                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = msg, Object = userProcess };
            }

            //20 - Sell crypto for fiat then 21
            if (userProcess.ProgressIndex == 20)
            {
                //Get the MRT exchange account
                var mrt = _userServ.GetByAddressID(userProcess.MRTAddressID);
                if (!mrt.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = mrt.Code, Message = mrt.Message };

                var sell = _exchangeServ.Sell(mrt.Object.ID, mrtContributeBTC + lenderBTC);
                if (!sell.Success)
                    return new ServiceResult<UserProcess> { Success = false, Code = sell.Code, Message = sell.Message };

                userProcess.ProgressIndex = 21;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "MRT crypto sold for fiat", Object = userProcess };
            }

            //21 - Done
            if (userProcess.ProgressIndex == 21)
            {
                userProcess.IsComplete = true;
                userProcess.StatusID = 2;
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "LockGains Start Process complete", Object = userProcess };
            }

            return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "Run through?", Object = userProcess };
        }

        private ServiceResult<UserProcess> End(UserProcess userProcess)
        {
            return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "NOT IMPLEMENTED", Object = userProcess };
        }

        
        private ServiceResult<Address> UserMRTMultisig(UserProcess userProcess)
        {
            var userWallets = _userWalletServ.GetByAddressID(userProcess.UserAddressID, UserWalletService.AddressOwner.User);
            if (!userWallets.Success || userWallets.Object.Count != 1)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "User wallet not found" };
            var userWallet = userWallets.Object.FirstOrDefault();

            var mrtWallets = _userWalletServ.GetByAddressID(userProcess.MRTAddressID, UserWalletService.AddressOwner.MRT);
            if (!mrtWallets.Success || mrtWallets.Object.Count != 1)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "MRT wallet not found" };
            var mrtWallet = mrtWallets.Object.FirstOrDefault();

            var result = _addressServ.Create(new AddressCreateRequest
            {
                MinSigsRequired = 2,
                UserWalletIDs = new List<int> { userWallet.ID, mrtWallet.ID },
                Label = "Investment",
                Description = "User & MRT (2-of-2)"
            });
            if (!result.Success)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "User & MRT address creation failed" };

            return new ServiceResult<Address> { Success = true, Code = 1, Message = "Success", Object = result.Object };
        }
        private ServiceResult<Address> MRTLenderMultisig(UserProcess userProcess)
        {
            var mrtWallets = _userWalletServ.GetByAddressID(userProcess.MRTAddressID, UserWalletService.AddressOwner.MRT);
            if (!mrtWallets.Success || mrtWallets.Object.Count != 1)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "MRT wallet not found" };
            var mrtWallet = mrtWallets.Object.FirstOrDefault();

            var lenderWallets = _userWalletServ.GetByAddressID(userProcess.MRTAddressID, UserWalletService.AddressOwner.Lender);
            if (!lenderWallets.Success || lenderWallets.Object.Count != 1)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Lender wallet not found" };
            var lenderWallet = lenderWallets.Object.FirstOrDefault();

            var result = _addressServ.Create(new AddressCreateRequest
            {
                MinSigsRequired = 2,
                UserWalletIDs = new List<int> { mrtWallet.ID, lenderWallet.ID },
                Label = "Hedge",
                Description = "MRT & Lender (2-of-2)"
            });
            if (!result.Success)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "MRT & Lender address creation failed" };

            return new ServiceResult<Address> { Success = true, Code = 1, Message = "Success", Object = result.Object };
        }
    }
}
