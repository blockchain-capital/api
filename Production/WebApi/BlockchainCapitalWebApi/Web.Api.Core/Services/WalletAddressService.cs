﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class WalletAddressService : IWalletAddressService
    {
        private readonly IWalletAddressRepository _walletAddressRepo;
        private readonly IUserWalletService _userWalletServ;
        private readonly IMapper _mapper;

        public WalletAddressService(IWalletAddressRepository walletAddressRepo, IUserWalletService userWalletServ, IMapper mapper)
        {
            _walletAddressRepo = walletAddressRepo;
            _userWalletServ = userWalletServ;
            _mapper = mapper;
        }

        public ServiceResult<List<WalletAddress>> GetAll()
        {
            var walletAddresses = _walletAddressRepo.GetAll().Result.Select(o => _mapper.Map<WalletAddress>(o)).ToList();

            if (walletAddresses == null)
                return new ServiceResult<List<WalletAddress>> { Success = false, Code = -1, Message = "Failed to get all wallet addresses" };

            return new ServiceResult<List<WalletAddress>> { Success = true, Code = 1, Object = walletAddresses };
        }
        public ServiceResult<List<WalletAddress>> GetByUserID(int userId)
        {
            //Get the user wallets
            var userWallets = _userWalletServ.GetAll(userId);
            if (userWallets == null)
                return new ServiceResult<List<WalletAddress>> { Success = false, Code = -1, Message = "Failed to get user wallets for user id " + userId };

            var walletAddresses = _walletAddressRepo.Where(o => userWallets.Object.Any(x=>x.ID == o.UserWalletID)).Select(o => _mapper.Map<WalletAddress>(o)).ToList();
            if (walletAddresses == null)
                return new ServiceResult<List<WalletAddress>> { Success = false, Code = -1, Message = "Failed to get user wallet addresses for user id " + userId };

            return new ServiceResult<List<WalletAddress>> { Success = true, Code = 1, Object = walletAddresses };
        }
        public ServiceResult<List<WalletAddress>> GetByUserWalletID(int userWalletId)
        {
            var walletAddresses = _walletAddressRepo.Where(o => o.UserWalletID == userWalletId).Select(o => _mapper.Map<WalletAddress>(o)).ToList();

            if (walletAddresses == null)
                return new ServiceResult<List<WalletAddress>> { Success = false, Code = -1, Message = "Failed to get user wallet addresses for user wallet id " + userWalletId };

            return new ServiceResult<List<WalletAddress>> { Success = true, Code = 1, Object = walletAddresses };
        }
        public ServiceResult<List<WalletAddress>> GetByAddressID(int addressId)
        {
            var walletAddresses = _walletAddressRepo.Where(o => o.AddressID == addressId).Select(o => _mapper.Map<WalletAddress>(o)).ToList();

            if (walletAddresses == null)
                return new ServiceResult<List<WalletAddress>> { Success = false, Code = -1, Message = "Failed to get user wallet addresses for address id " + addressId };

            return new ServiceResult<List<WalletAddress>> { Success = true, Code = 1, Object = walletAddresses };
        }
        public ServiceResult<WalletAddress> Get(int id)
        {
            var o = _walletAddressRepo.GetById(id).Result;
            if (o == null || o.ID == 0)
                return new ServiceResult<WalletAddress> { Success = false, Code = -1, Message = "Wallet address not found" };

            return new ServiceResult<WalletAddress> { Success = true, Code = 1, Object = _mapper.Map<WalletAddress>(o) };
        }
        public ServiceResult<WalletAddress> Create(WalletAddressCreateRequest request)
        {
            if (request.UserWalletID == 0)
                return new ServiceResult<WalletAddress> { Success = false, Code = -1, Message = "User wallet ID not set" };
            if (request.AddressID == 0)
                return new ServiceResult<WalletAddress> { Success = false, Code = -1, Message = "Address ID not set" };

            var found = this.WalletAddressExist(request.UserWalletID, request.AddressID);

            if (found != null)
                return new ServiceResult<WalletAddress> { Success = false, Code = 2, Message = "Wallet address already exists", Object = found };

            Infrastructure.Models.WalletAddress newWalletAddress = new Infrastructure.Models.WalletAddress
            {
                UserWalletID = request.UserWalletID,
                AddressID = request.AddressID,
                UserAddressID = request.UserAddressID
            };

            var id = _walletAddressRepo.Insert(newWalletAddress).Result;
            if (id == 0)
                return new ServiceResult<WalletAddress> { Success = false, Code = -1, Message = "Wallet address insert failed" };

            return new ServiceResult<WalletAddress> { Success = true, Code = 1, Message = "Wallet address created", Object = _mapper.Map<WalletAddress>(_walletAddressRepo.GetById(id).Result) };
        }

        private WalletAddress WalletAddressExist(int userWalletId, int addressId)
        {
            var found = _walletAddressRepo.Where(o => o.UserWalletID == userWalletId && o.AddressID == addressId).FirstOrDefault();

            return _mapper.Map<WalletAddress>(found);
        }
    }
}
