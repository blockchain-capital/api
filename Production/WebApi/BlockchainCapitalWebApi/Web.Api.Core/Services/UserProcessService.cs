﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class UserProcessService : IUserProcessService
    {
        private readonly IUserProcessRepository _userProcessRepo;
        private readonly IUserService _userServ;
        private readonly IWalletAddressService _walletAddressServ;
        private readonly IAddressService _addressServ;
        private readonly IMapper _mapper;

        public UserProcessService(IUserProcessRepository userProcessRepo, IUserService userServ, IWalletAddressService walletAddressServ, IAddressService addressServ, IMapper mapper)
        {
            _userProcessRepo = userProcessRepo;
            _userServ = userServ;
            _walletAddressServ = walletAddressServ;
            _addressServ = addressServ;
            _mapper = mapper;
        }

        public ServiceResult<List<UserProcess>> GetAll()
        {
            var userProcesses = _userProcessRepo.GetAll().Result.Select(o => _mapper.Map<UserProcess>(o)).ToList();

            if (userProcesses == null)
                return new ServiceResult<List<UserProcess>> { Success = false, Code = -1, Message = "Failed to find all user process" };

            return new ServiceResult<List<UserProcess>> { Success = true, Code = 1, Object = userProcesses };
        }
        public ServiceResult<List<UserProcess>> GetAll(int userId)
        {
            var walletAddresses = _walletAddressServ.GetByUserID(userId);
            if (walletAddresses == null)
                return new ServiceResult<List<UserProcess>> { Success = false, Code = -1, Message = "Failed to get wallet address for user id " + userId };

            var userProcesses = _userProcessRepo.Where(o => walletAddresses.Object.Any(x=>x.AddressID == o.UserAddressID)).Select(o => _mapper.Map<UserProcess>(o)).ToList();
            if (userProcesses == null)
                return new ServiceResult<List<UserProcess>> { Success = false, Code = -1, Message = "Failed to get user processes for user id " + userId };

            return new ServiceResult<List<UserProcess>> { Success = true, Code = 1, Object = userProcesses };
        }
        public ServiceResult<UserProcess> Get(int id)
        {
            var userProcess = _userProcessRepo.GetById(id).Result;

            if (userProcess == null || userProcess.ID == 0)
                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "Failed to find user process" };

            return new ServiceResult<UserProcess> { Success = true, Code = 1, Object = _mapper.Map<UserProcess>(userProcess) };
        }
        public ServiceResult<UserProcess> Create(UserProcessCreateRequest request)
        {
            var found = this.UserProcessExist(request.ProcessID, request.UserAddressID);

            if (found != null)
                return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Already exists" };

            Infrastructure.Models.UserProcess newUser = new Infrastructure.Models.UserProcess
            {
                ProcessID = request.ProcessID,
                UserAddressID = request.UserAddressID,
                MRTAddressID = request.MRTAddressID,
                LenderAddressID = request.LenderAddressID,
                StatusID = 1,
                Description = request.Description,
                Name = request.Name,
                InitialAmount = request.InitialAmount
            };

            var id = _userProcessRepo.Insert(newUser).Result;
            if (id == 0)
                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "User process insert failed" };

            return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "Created", Object = _mapper.Map<UserProcess>(_userProcessRepo.GetById(id).Result) };
        }
        public ServiceResult<UserProcess> Update(UserProcessUpdateRequest request)
        {
            var found = _userProcessRepo.GetById(request.ID).Result;
            if (found == null)
                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "User process not found" };

            found.StatusID = request.StatusID;
            found.ProgressIndex = request.ProgressIndex;
            found.ProgressMessage = request.ProgressMessage;
            found.HasError = request.HasError;
            found.IsComplete = request.IsComplete;

            _userProcessRepo.Update(found);

            return new ServiceResult<UserProcess> { Success = true, Code = 1, Message = "User process updated", Object = _mapper.Map<UserProcess>(_userProcessRepo.GetById(request.ID).Result) };
        }

        private UserProcess UserProcessExist(int processId, int addressId)
        {
            var found = _userProcessRepo.Where(o => o.ProcessID == processId && o.UserAddressID == addressId && o.StatusID == 1).FirstOrDefault();

            return _mapper.Map<UserProcess>(found);
        }
    }
}
