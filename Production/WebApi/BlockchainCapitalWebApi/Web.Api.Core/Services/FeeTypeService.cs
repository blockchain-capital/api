﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class FeeTypeService : IFeeTypeService
    {
        private readonly IFeeTypeRepository _feeTypeRepo;
        private readonly IMapper _mapper;

        public FeeTypeService(IFeeTypeRepository feeTypeRepo, IMapper mapper)
        {
            _feeTypeRepo = feeTypeRepo;
            _mapper = mapper;
        }

        public ServiceResult<List<FeeType>> GetAll()
        {
            var feeTypes = _feeTypeRepo.GetAll().Result.Select(o => _mapper.Map<FeeType>(o)).ToList();

            if (feeTypes == null)
                return new ServiceResult<List<FeeType>> { Success = false, Code = -1, Message = "Failed to find all fee types" };

            return new ServiceResult<List<FeeType>> { Success = true, Code = 1, Object = feeTypes };
        }
        public ServiceResult<FeeType> Get(int id)
        {
            var feeType = _feeTypeRepo.GetById(id).Result;

            if (feeType == null || feeType.ID == 0)
                return new ServiceResult<FeeType> { Success = false, Code = -1, Message = "Failed to find fee type" };

            return new ServiceResult<FeeType> { Success = true, Code = 1, Object = _mapper.Map<FeeType>(feeType) };
        }
        public ServiceResult<FeeType> GetByCode(string code)
        {
            var feeTypes = _feeTypeRepo.GetAll().Result; //TODO: Predicate
            if (feeTypes == null)
                return new ServiceResult<FeeType> { Success = false, Code = -1, Message = "Failed to find fee types" };

            var feeType = feeTypes.FirstOrDefault(o => o.Code.ToLower() == code.ToLower());
            if (feeType == null || feeType.ID == 0)
                return new ServiceResult<FeeType> { Success = false, Code = -1, Message = "Failed to find fee type" };

            return new ServiceResult<FeeType> { Success = true, Code = 1, Object = _mapper.Map<FeeType>(feeType) };
        }
        public ServiceResult<FeeType> Create(FeeTypeCreateRequest request)
        {
            var found = this.FeeTypeExist(request.Code);

            if (found != null)
                return new ServiceResult<FeeType> { Success = true, Code = 1, Message = "Already exists" };

            Infrastructure.Models.FeeType newFeeType = new Infrastructure.Models.FeeType
            {
                Code = request.Code,
                Name = request.Name,
                Description = request.Description,
                CurrencyID = request.CurrencyID,
                IsMandatory = request.IsMandatory,
                Percentage = request.Percentage,
                Amount = request.Amount
            };

            var id = _feeTypeRepo.Insert(newFeeType).Result;
            if (id == 0)
                return new ServiceResult<FeeType> { Success = false, Code = -1, Message = "Fee type insert failed" };

            return new ServiceResult<FeeType> { Success = true, Code = 1, Message = "Created", Object = _mapper.Map<FeeType>(_feeTypeRepo.GetById(id).Result) };
        }

        private FeeType FeeTypeExist(string code)
        {
            var found = _feeTypeRepo.Where(o => o.Code.ToLower() == code.ToLower()).FirstOrDefault();

            return _mapper.Map<FeeType>(found);
        }
    }
}
