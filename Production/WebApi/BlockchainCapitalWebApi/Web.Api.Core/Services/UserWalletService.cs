﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class UserWalletService : IUserWalletService
    {
        private readonly IUserWalletRepository _userWalletRepo;
        private readonly IUserRepository _userRepo;
        private readonly IWalletAddressRepository _walletAddressRepo;
        private readonly IBlockchainService _blockchainServ;
        private readonly IMapper _mapper;

        public enum AddressOwner
        {
            Any = 1,
            User = 2,
            MRT = 3,
            Lender = 4,
            Exchange = 5,
            NonExchange = 6
        }

        public UserWalletService(IUserWalletRepository userWalletRepo, IUserRepository userRepo, IWalletAddressRepository walletAddressRepo, IBlockchainService blockchainServ, IMapper mapper)
        {
            _userWalletRepo = userWalletRepo;
            _userRepo = userRepo;
            _walletAddressRepo = walletAddressRepo;
            _blockchainServ = blockchainServ;
            _mapper = mapper;
        }

        public ServiceResult<List<UserWallet>> GetAll()
        {
            var wallets = _userWalletRepo.GetAll().Result.Select(o => _mapper.Map<UserWallet>(o)).ToList();

            if (wallets == null)
                return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get all wallets" };

            return new ServiceResult<List<UserWallet>> { Success = true, Code = 1, Object = wallets };
        }
        public ServiceResult<List<UserWallet>> GetAll(int userId)
        {
            var wallets = _userWalletRepo.Where(o => o.UserID == userId).Select(o => _mapper.Map<UserWallet>(o)).ToList();

            if (wallets == null)
                return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get user wallets for id " + userId };

            return new ServiceResult<List<UserWallet>> { Success = true, Code = 1, Object = wallets };
        }
        public ServiceResult<List<UserWallet>> GetByAddressID(int addressId, AddressOwner owner = AddressOwner.Any)
        {
            var walletAddresses = _walletAddressRepo.Where(o => o.AddressID == addressId).ToList();
            if (walletAddresses == null || walletAddresses.Count == 0)
                return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get wallet address for address id " + addressId };

            if (owner == AddressOwner.Any)
            {
                //Get all user wallets (could be multiple for multisig)
                var userWallets = _userWalletRepo.Where(o => walletAddresses.Any(x => x.UserWalletID == o.ID)).Select(o => _mapper.Map<UserWallet>(o)).ToList();
                if (userWallets != null && userWallets.Count == 0)
                    return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get user wallets for address id " + addressId };

                return new ServiceResult<List<UserWallet>> { Success = true, Code = 1, Object = userWallets };
            }

            var walletOwners = new List<User>();
            //Get all users that own this address
            foreach (var walletAddress in walletAddresses)
            {
                var userWallet = this.Get(walletAddress.UserWalletID.Value);
                if (!userWallet.Success)
                    return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get user wallet for id " + walletAddress.UserWalletID.Value };

                //Get the user for this wallet
                var user = _userRepo.GetById(userWallet.Object.UserID).Result;
                if (user == null || user.ID == 0)
                    return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "Failed to get user" };

                if (owner == AddressOwner.NonExchange)
                {
                    if (user.UserTypeID != 5)
                        return new ServiceResult<List<UserWallet>> { Success = true, Code = 1, Object = new List<UserWallet> { userWallet.Object } };
                }
                else if (user.UserTypeID == (int)owner)
                {
                    return new ServiceResult<List<UserWallet>> { Success = true, Code = 1, Object = new List<UserWallet> { userWallet.Object } };
                }
            }

            return new ServiceResult<List<UserWallet>> { Success = false, Code = -1, Message = "No wallets found" };
        }
        public ServiceResult<UserWallet> Get(int id)
        {
            var o = _userWalletRepo.GetById(id).Result;
            if (o == null || o.ID == 0)
                return new ServiceResult<UserWallet> { Success = false, Code = -1, Message = "Wallet not found" };

            return new ServiceResult<UserWallet> { Success = true, Code = 1, Object = _mapper.Map<UserWallet>(o) };
        }
        public ServiceResult<UserWallet> Create(UserWalletCreateRequest request)
        {
            if (request.UserID == 0)
                return new ServiceResult<UserWallet> { Success = false, Code = -1, Message = "User ID not set" };

            var found = this.UserWalletExist(request.UserID, request.Label);

            if (found != null)
                return new ServiceResult<UserWallet> { Success = false, Code = 2, Message = "Wallet already exists", Object = found };

            Infrastructure.Models.UserWallet newUserWallet = new Infrastructure.Models.UserWallet
            {
                Label = request.Label,
                Description = request.Description,
                UserID = request.UserID
            };

            if (string.IsNullOrEmpty(request.PassPhrase))
            {
                var mnemoResult = _blockchainServ.GenerateMnemo();
                if (mnemoResult.Success)
                    newUserWallet.PassPhrase = String.Join(" ", mnemoResult.Object, 0, mnemoResult.Object.Length);
                else
                    return new ServiceResult<UserWallet> { Success = false, Code = mnemoResult.Code, Message = mnemoResult.Message };
            }
            else
            {
                newUserWallet.PassPhrase = request.PassPhrase;
            }
            newUserWallet.SCode = Helper.GeneratePassword(10);

            var id = _userWalletRepo.Insert(newUserWallet).Result;
            if (id == 0)
                return new ServiceResult<UserWallet> { Success = false, Code = -1, Message = "User wallet insert failed" };

            return new ServiceResult<UserWallet> { Success = true, Code = 1, Message = "User wallet created", Object = _mapper.Map<UserWallet>(_userWalletRepo.GetById(id).Result) };
        }

        private UserWallet UserWalletExist(int userId, string label)
        {
            var found = _userWalletRepo.Where(o => o.UserID == userId && o.Label.ToLower() == label.ToLower()).FirstOrDefault();

            return _mapper.Map<UserWallet>(found);
        }
    }
}
