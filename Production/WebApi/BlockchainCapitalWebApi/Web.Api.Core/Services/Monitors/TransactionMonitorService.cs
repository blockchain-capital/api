﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using AutoMapper;
using Web.Api.Core.Models.Dto;
using Web.Api.Core.Interfaces.Monitors;
using System.Linq;

namespace Web.Api.Core.Services.Monitors
{
    public class TransactionMonitorService : ITransactionMonitorService
    {
        private readonly IConfigService _configServ;
        private readonly IFeeTypeService _feeTypeServ;
        private readonly ITransactionService _transactionServ;
        private readonly IAddressService _addressServ;
        private readonly IUserWalletService _userWalletServ;
        private readonly IExchangeService _exchangeServ;
        private readonly IBlockchainService _blockchainServ;
        private readonly IMapper _mapper;

        public TransactionMonitorService(IConfigService configServ, IFeeTypeService feeTypeServ, ITransactionService transactionServ, IAddressService addressServ, IUserWalletService userWalletServ, IExchangeService exchangeServ, 
            IBlockchainService blockchainServ, IMapper mapper)
        {
            _configServ = configServ;
            _feeTypeServ = feeTypeServ;
            _transactionServ = transactionServ;
            _addressServ = addressServ;
            _userWalletServ = userWalletServ;
            _exchangeServ = exchangeServ;
            _blockchainServ = blockchainServ;
            _mapper = mapper;
        }

        public ServiceResult<List<Transaction>> Monitor()
        {
            var results = new List<Transaction>();

            var trxs = _transactionServ.GetAll(o => o.StatusID == 1);
            if (!trxs.Success)
                return new ServiceResult<List<Transaction>> { Success = false, Code = trxs.Code, Message = trxs.Message };

            foreach (var trx in trxs.Object)
            {
                var result = this.Monitor(trx.ID);
                if (!result.Success)
                    return new ServiceResult<List<Transaction>> { Success = false, Code = result.Code, Message = result.Message };

                results.Add(result.Object);
            }

            return new ServiceResult<List<Transaction>> { Success = true, Code = 1, Object = results };
        }

        public ServiceResult<Transaction> Monitor(int trxId)
        {
            var trx = _transactionServ.Get(trxId);
            if (!trx.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Transaction not found" };

            if (trx.Object.StatusID != 1)
                return new ServiceResult<Transaction> { Success = true, Code = 2, Message = "Transaction already completed", Object = trx.Object };

            var result = this.Send(trx.Object);
            if (!result.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = result.Message };

            return new ServiceResult<Transaction> { Success = true, Code = 1, Object = _transactionServ.Get(trxId).Object };
        }

        private ServiceResult<Transaction> Send(Transaction trx)
        {
            if (trx.TypeID == TransactionType.BuyFromExchange)
            {
                //The exchange is in fact sending the crypto to the user - so swop around sender and recipient
                var senderAddressID = trx.SenderAddressID;
                var recipientAddressID = trx.RecipientAddressID;

                trx.SenderAddressID = recipientAddressID;
                trx.RecipientAddressID = senderAddressID;
            }

            var senderBalanceFiat = _exchangeServ.Balance(trx.SenderAddressID, ExchangeService.ExchangeType.Fiat).Object;
            var senderBalanceCrypto = _exchangeServ.Balance(trx.SenderAddressID, ExchangeService.ExchangeType.Crypto).Object;

            //Get the sender and recipient
            var senderAddress = _addressServ.Get(trx.SenderAddressID);
            if (!senderAddress.Success)
                return new ServiceResult<Transaction> { Success = false, Code = senderAddress.Code, Message = senderAddress.Message };
            var recipientAddress = _addressServ.Get(trx.RecipientAddressID);
            if (!recipientAddress.Success)
                return new ServiceResult<Transaction> { Success = false, Code = recipientAddress.Code, Message = recipientAddress.Message };

            //Get miner fee
            var minerFee = _feeTypeServ.GetByCode("mf");
            if (!minerFee.Success)
                return new ServiceResult<Transaction> { Success = false, Code = minerFee.Code, Message = minerFee.Message };

            //These are normal address to address payments - could be multisig to single or single to multisig
            var senderWallets = _userWalletServ.GetByAddressID(trx.SenderAddressID);
            if (!senderWallets.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Failed to get sender wallet(s)" };

            var pks = this.GetPrivateKeys(senderWallets.Object);
            if (!pks.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Failed to get private key(s)" };

            var send = new ServiceResult<SendResult>();
            if (pks.Object.Count == 1)
                send = _blockchainServ.Send(pks.Object.FirstOrDefault(), trx.AmountBTC, recipientAddress.Object.Addr, minerFee.Object.Amount, _configServ.Network);
            else
                send = _blockchainServ.Send(pks.Object, trx.AmountBTC, recipientAddress.Object.Addr, minerFee.Object.Amount, 2, _configServ.Network);

            if (!send.Success)
            {
                //Update trx with error status
                _transactionServ.Update(new TransactionUpdateRequest { ID = trx.ID, AmountBTC = trx.AmountBTC, Message = send.Message });
                return new ServiceResult<Transaction> { Success = false, Code = send.Code, Message = send.Message };
            }

            //Update trx with success
            var trxUpdate = _transactionServ.Update(new TransactionUpdateRequest { ID = trx.ID, AmountBTC = trx.AmountBTC, Message = "Sent", StatusID = trx.StatusID + 1 });
            if (!trxUpdate.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Transaction update failed " };

            //Update fiat balance
            var update = new ServiceResult<Address>();
            if (trx.TypeID == TransactionType.SellToExchange)
                update = _addressServ.Update(new AddressUpdateRequest { Fiat = senderBalanceFiat + trx.Amount });
            else if (trx.TypeID == TransactionType.BuyFromExchange)
                update = _addressServ.Update(new AddressUpdateRequest { Fiat = senderBalanceFiat - trx.Amount });

            return new ServiceResult<Transaction> { Success = true, Code = 1, Object = _transactionServ.Get(trx.ID).Object };
        }
        private ServiceResult<List<string>> GetPrivateKeys(List<UserWallet> userWallets)
        {
            if (userWallets.Count == 0)
                return new ServiceResult<List<string>> { Success = false, Code = -1, Message = "No user wallets" };

            var pks = new List<string>();

            foreach (var userWallet in userWallets)
            {
                var addr = _addressServ.GetByUserWalletID(userWallet.ID, AddressService.Type.Seed);
                if (!addr.Success || addr.Object.Count != 1)
                    return new ServiceResult<List<string>> { Success = false, Code = -1, Message = "Failed to get seed address" };

                var pk = _blockchainServ.GetPrivateKey(userWallet.PassPhrase, addr.Object.FirstOrDefault().KeyNumber, _configServ.Network);
                if (!pk.Success)
                    return new ServiceResult<List<string>> { Success = false, Code = -1, Message = "Failed to get private key" };

                pks.Add(pk.Object);
            }

            return new ServiceResult<List<string>> { Success = true, Code = 1, Object = pks };
        }
    }
}
