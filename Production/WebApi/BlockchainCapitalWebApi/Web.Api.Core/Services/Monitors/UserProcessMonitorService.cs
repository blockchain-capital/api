﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using AutoMapper;
using Web.Api.Core.Models.Dto;
using Web.Api.Core.Interfaces.Monitors;
using Web.Api.Core.Interfaces.Processes;

namespace Web.Api.Core.Services.Monitors
{
    public class UserProcessMonitorService : IUserProcessMonitorService
    {
        private readonly IConfigService _configServ;
        private readonly IUserProcessService _userProcessServ;
        private readonly ILockGainsService _lockGainsServ;
        private readonly IMapper _mapper;

        public UserProcessMonitorService(IConfigService configServ, IUserProcessService userProcessServ, ILockGainsService lockGainsServ, IMapper mapper)
        {
            _configServ = configServ;
            _userProcessServ = userProcessServ;
            _lockGainsServ = lockGainsServ;
            _mapper = mapper;
        }

        public ServiceResult<List<UserProcess>> Monitor()
        {
            var results = new List<UserProcess>();

            var ups = _userProcessServ.GetAll();
            if (!ups.Success)
                return new ServiceResult<List<UserProcess>> { Success = false, Code = ups.Code, Message = ups.Message };

            foreach (var up in ups.Object)
            {
                var result = this.Monitor(up.ID);
                if (!result.Success)
                    return new ServiceResult<List<UserProcess>> { Success = false, Code = result.Code, Message = result.Message };

                results.Add(result.Object);
            }

            return new ServiceResult<List<UserProcess>> { Success = true, Code = 1, Object = results };
        }

        public ServiceResult<UserProcess> Monitor(int userProcessId)
        {
            var userProcess = _userProcessServ.Get(userProcessId);
            if (!userProcess.Success)
                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = "User process not found" };

            if (userProcess.Object.IsComplete)
                return new ServiceResult<UserProcess> { Success = true, Code = 2, Message = "User process already completed", Object = userProcess.Object };

            var result = _lockGainsServ.Init(userProcessId);
            if (!result.Success)
                return new ServiceResult<UserProcess> { Success = false, Code = -1, Message = result.Message };

            return new ServiceResult<UserProcess> { Success = true, Code = 1, Object = _userProcessServ.Get(userProcessId).Object };
        }
    }
}