﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;

namespace Web.Api.Core.Services
{
    public class ConfigService : IConfigService
    {
        public NBitcoin.Network Network { get { return NBitcoin.Network.TestNet; } }
        public int ExchangeUserID { get { return 8; } }

        private readonly IOptionService _optionServ;

        public ConfigService(IOptionService optionServ)
        {
            _optionServ = optionServ;
        }

        public ServiceResult<decimal> ExchangeRate(string curr)
        {
            var btcz = _optionServ.GetByCode("btcz");
            if (!btcz.Success)
                return new ServiceResult<decimal> { Success = false, Code = -1, Message = btcz.Message };

            if (btcz.Object.ValueNumeric > 0)
                return new ServiceResult<decimal> { Success = true, Code = 1, Object = btcz.Object.ValueNumeric };
            
            //TODO: Implement other currencies

            //if (this._cache.TryGetValue(curr, out exchRate))
            //{
            //    return true;
            //}
            //else
            {
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString("https://api.mybitx.com/api/1/ticker?pair=XBTZAR");
                    var data = (JObject)JsonConvert.DeserializeObject(json);
                    var exchRate = data["last_trade"].Value<decimal>();

                    if (exchRate > 0)
                    {
                        //this._cache.Set(curr, exchRate, _cacheTime);

                        return new ServiceResult<decimal> { Success = true, Code = 1, Object = exchRate };
                    }
                }
            }

            return new ServiceResult<decimal> { Success = false, Code = -1, Message = "Failed to get exchange rate" };
        }
    }
}
