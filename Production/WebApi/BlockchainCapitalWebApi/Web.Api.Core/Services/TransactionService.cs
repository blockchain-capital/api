﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepo;
        private readonly IBlockchainService _blockchainServ;
        private readonly IMapper _mapper;

        public TransactionService(ITransactionRepository transactionRepo, IBlockchainService blockchainServ, IMapper mapper)
        {
            _transactionRepo = transactionRepo;
            _blockchainServ = blockchainServ;
            _mapper = mapper;
        }

        public ServiceResult<List<Transaction>> GetAll()
        {
            return new ServiceResult<List<Transaction>> { Success = false, Code = -2, Message = "NOT IMPLEMENTED" };
        }
        public ServiceResult<List<Transaction>> GetAll(Expression<Func<Infrastructure.Models.Transaction, bool>> exp)
        {
            var trxs = _transactionRepo.Where(exp).Select(o => _mapper.Map<Transaction>(o)).ToList();
            if (trxs == null)
                return new ServiceResult<List<Transaction>> { Success = false, Code = -1, Message = "Failed to get transactions" };

            return new ServiceResult<List<Transaction>> { Success = true, Code = 1, Object = trxs };
        }
        public ServiceResult<List<Transaction>> GetAll(int addressId)
        {
            var trxs = _transactionRepo.Where(o => o.SenderAddressID == addressId || o.RecipientAddressID == addressId).Select(o => _mapper.Map<Transaction>(o)).ToList();
            if (trxs == null)
                return new ServiceResult<List<Transaction>> { Success = false, Code = -1, Message = "Failed to get transactions for address id " + addressId};

            return new ServiceResult<List<Transaction>> { Success = true, Code = 1, Object = trxs };
        }
        public ServiceResult<Transaction> Get(int id)
        {
            var o = _transactionRepo.GetById(id).Result;
            if (o == null || o.ID == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Transaction not found" };

            return new ServiceResult<Transaction> { Success = true, Code = 1, Object = _mapper.Map<Transaction>(o) };
        }
        public ServiceResult<Transaction> Create(TransactionCreateRequest request)
        {
            if (request.SenderAddressID == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Sender address ID not set" };
            if (request.RecipientAddressID == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Recipient address ID not set" };
            //if (request.Amount == 0)
            //    return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Amount not set" };

            var found = this.TransactionExist(request.SenderAddressID, request.RecipientAddressID, request.Amount);

            if (found != null)
                return new ServiceResult<Transaction> { Success = false, Code = 2, Message = "Transaction already exists", Object = found };

            Infrastructure.Models.Transaction newTransaction = new Infrastructure.Models.Transaction
            {
                TypeID = (int)request.Type,
                StatusID = 1,
                SenderAddressID = request.SenderAddressID,
                RecipientAddressID = request.RecipientAddressID,
                UserProcessID = request.UserProcessID,
                Amount = request.Amount,
                AmountBTC = request.AmountBTC
            };

            var id = _transactionRepo.Insert(newTransaction).Result;
            if (id == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Transaction insert failed" };

            return new ServiceResult<Transaction> { Success = true, Code = 1, Message = "Transaction created", Object = _mapper.Map<Transaction>(_transactionRepo.GetById(id).Result) };
        }
        public ServiceResult<Transaction> Update(TransactionUpdateRequest request)
        {
            var found = _transactionRepo.GetById(request.ID).Result;
            if (found == null)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Transaction not found exists" };

            found.StatusID = request.StatusID;
            found.Message = request.Message;

            _transactionRepo.Update(found);

            return new ServiceResult<Transaction> { Success = true, Code = 1, Message = "Transaction updated", Object = _mapper.Map<Transaction>(_transactionRepo.GetById(request.ID).Result) };
        }

        private Transaction TransactionExist(int senderAddressId, int recipientAddressId, decimal amount)
        {
            var found = _transactionRepo.Where(o => o.SenderAddressID == senderAddressId && o.RecipientAddressID == recipientAddressId && o.Amount == amount && o.Added >= DateTime.Now.AddMinutes(-1)).FirstOrDefault();

            return _mapper.Map<Transaction>(found);
        }
    }
}
