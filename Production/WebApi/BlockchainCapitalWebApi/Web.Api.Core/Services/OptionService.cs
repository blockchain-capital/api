﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepo;
        private readonly IMapper _mapper;

        public OptionService(IOptionRepository optionRepo, IMapper mapper)
        {
            _optionRepo = optionRepo;
            _mapper = mapper;
        }

        public ServiceResult<List<Option>> GetAll()
        {
            var options = _optionRepo.GetAll().Result.Select(o => _mapper.Map<Option>(o)).ToList();

            if (options == null)
                return new ServiceResult<List<Option>> { Success = false, Code = -1, Message = "Failed to find all options" };

            return new ServiceResult<List<Option>> { Success = true, Code = 1, Object = options };
        }
        public ServiceResult<Option> Get(int id)
        {
            var option = _optionRepo.GetById(id).Result;

            if (option == null || option.ID == 0)
                return new ServiceResult<Option> { Success = false, Code = -1, Message = "Failed to find option" };

            return new ServiceResult<Option> { Success = true, Code = 1, Object = _mapper.Map<Option>(option) };
        }
        public ServiceResult<Option> GetByCode(string code)
        {
            var options = _optionRepo.GetAll().Result; //TODO: Predicate
            if (options == null)
                return new ServiceResult<Option> { Success = false, Code = -1, Message = "Failed to find options" };

            var option = options.FirstOrDefault(o => o.Code.ToLower() == code.ToLower());
            if (option == null || option.ID == 0)
                return new ServiceResult<Option> { Success = false, Code = -1, Message = "Failed to find option" };

            return new ServiceResult<Option> { Success = true, Code = 1, Object = _mapper.Map<Option>(option) };
        }
        public ServiceResult<Option> Create(OptionCreateRequest request)
        {
            var found = this.OptionExist(request.Code);

            if (found != null)
                return new ServiceResult<Option> { Success = true, Code = 1, Message = "Already exists" };

            Infrastructure.Models.Option newOption = new Infrastructure.Models.Option
            {
                Code = request.Code,
                Name = request.Name,
                Description = request.Description,
                ValueNumeric = request.ValueNumeric,
                ValueText = request.ValueText
            };

            var id = _optionRepo.Insert(newOption).Result;
            if (id == 0)
                return new ServiceResult<Option> { Success = false, Code = -1, Message = "Option insert failed" };

            return new ServiceResult<Option> { Success = true, Code = 1, Message = "Created", Object = _mapper.Map<Option>(_optionRepo.GetById(id).Result) };
        }

        private Option OptionExist(string code)
        {
            var found = _optionRepo.Where(o => o.Code.ToLower() == code.ToLower()).FirstOrDefault();

            return _mapper.Map<Option>(found);
        }
    }
}
