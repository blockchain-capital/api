﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class AddressService : IAddressService
    {
        private readonly IConfigService _configServ;
        private readonly IAddressRepository _addressRepo;
        private readonly IUserService _userServ;
        private readonly IUserWalletService _userWalletServ;
        private readonly IWalletAddressService _walletAddressServ;
        private readonly IBlockchainService _blockchainServ;
        private readonly IMapper _mapper;

        public enum Type
        {
            Any = 1,
            Seed = 2,
            Exchange = 3,
            Multisig = 4
        }
        public AddressService(IConfigService configServ, IAddressRepository addressRepo, IUserService userServ, IUserWalletService userWalletServ, IWalletAddressService walletAddressServ, IBlockchainService blockchainServ, IMapper mapper)
        {
            _configServ = configServ;
            _addressRepo = addressRepo;
            _userServ = userServ;
            _userWalletServ = userWalletServ;
            _walletAddressServ = walletAddressServ;
            _blockchainServ = blockchainServ;
            _mapper = mapper;
        }

        public ServiceResult<List<Address>> GetAll()
        {
            var addresses = _addressRepo.GetAll().Result.Select(o => _mapper.Map<Address>(o)).ToList();

            if (addresses == null)
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to get all addresses" };

            try
            {
                addresses.ForEach(o => { o = this.SetAdditional(o); });
            }
            catch (Exception)
            {
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to set additional data" };
            }

            return new ServiceResult<List<Address>> { Success = true, Code = 1, Object = addresses };
        }
        public ServiceResult<List<Address>> GetByUserWalletID(int userWalletId, Type type = Type.Any)
        {
            //Do a lookup to find the address to wallet mappings
            var walletAddresses = _walletAddressServ.GetByUserWalletID(userWalletId);
            if (!walletAddresses.Success)
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to get wallet address for user wallet id " + userWalletId };

            var addresses = _addressRepo.Where(o => walletAddresses.Object.Any(x => x.AddressID == o.ID)).Select(o => _mapper.Map<Address>(o)).ToList();
            if (addresses == null)
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to get addresses for user wallet id " + userWalletId };

            try
            {
                addresses.ForEach(o => { o = this.SetAdditional(o); });
                addresses = this.Filter(addresses, type);
            }
            catch (Exception)
            {
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to set additional data for user wallet id " + userWalletId };
            }

            return new ServiceResult<List<Address>> { Success = true, Code = 1, Object = addresses };
        }
        public ServiceResult<List<Address>> GetByUserID(int userId, Type type = Type.Any)
        {
            var walletAddresses = _walletAddressServ.GetByUserID(userId);
            if (!walletAddresses.Success)
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to get wallet addresses for user id " + userId };

            var addresses = _addressRepo.Where(o => walletAddresses.Object.Any(x => x.AddressID == o.ID)).Select(o => _mapper.Map<Address>(o)).ToList();
            if (addresses == null)
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to get addresses for user id " + userId };

            try
            {
                addresses.ForEach(o => { o = this.SetAdditional(o); });
                addresses = this.Filter(addresses, type);
            }
            catch (Exception)
            {
                return new ServiceResult<List<Address>> { Success = false, Code = -1, Message = "Failed to set additional data for user id " + userId };
            }

            return new ServiceResult<List<Address>> { Success = true, Code = 1, Object = addresses };
        }
        public ServiceResult<Address> Get(int id)
        {
            var o = _addressRepo.GetById(id).Result;
            if (o == null || o.ID == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address not found" };

            var addr = _mapper.Map<Address>(o);
            addr = this.SetAdditional(addr);

            return new ServiceResult<Address> { Success = true, Code = 1, Object = addr };
        }
        public ServiceResult<Address> Get(string address)
        {
            var a = _addressRepo.Where(o => o.Addr == address).FirstOrDefault();
            if (a == null || a.ID == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address not found" };

            var addr = _mapper.Map<Address>(a);
            addr = this.SetAdditional(addr);

            return new ServiceResult<Address> { Success = true, Code = 1, Object = addr };
        }
        public ServiceResult<Address> Create(AddressCreateRequest request)
        {
            if (request.UserWalletIDs.Count == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "User wallet id not set" };

            Infrastructure.Models.Address newAddress = new Infrastructure.Models.Address();
            if (request.UserWalletIDs.Count == 1)
            {
                var walletAddresses = _walletAddressServ.GetByUserWalletID(request.UserWalletIDs.FirstOrDefault());
                if (!walletAddresses.Success)
                    return new ServiceResult<Address> { Success = false, Code = -1, Message = "Failed to find wallet addresses" };

                var keyNumber = walletAddresses.Object.Count;

                var userWallet = _userWalletServ.Get(request.UserWalletIDs.FirstOrDefault());
                if (!userWallet.Success)
                    return new ServiceResult<Address> { Success = false, Code = -1, Message = "Failed to find user wallet" };

                var addressResponse = _blockchainServ.GenerateAddress(userWallet.Object.PassPhrase, keyNumber, NBitcoin.Network.TestNet);
                if (!addressResponse.Success)
                    return new ServiceResult<Address> { Success = false, Code = -1, Message = addressResponse.Message };

                var found = this.AddressExist(addressResponse.Object.Address);
                if (found != null)
                    return new ServiceResult<Address> { Success = true, Code = 2, Message = "Address already exists", Object = found };

                newAddress = new Infrastructure.Models.Address
                {
                    Label = request.Label,
                    Description = request.Description,
                    CurrencyID = request.CurrencyID,
                    KeyNumber = keyNumber,
                    Addr = addressResponse.Object.Address
                };
            }
            else
            {
                var pks = new List<string>();
                foreach (var userWalletID in request.UserWalletIDs)
                {
                    //Get seed address for this userwallet
                    var addr = this.GetByUserWalletID(userWalletID, Type.Seed);
                    if (!addr.Success)
                        return new ServiceResult<Address> { Success = false, Code = addr.Code, Message = addr.Message };

                    var pk = _userServ.PrivateKey(addr.Object.FirstOrDefault(o=>o.IsSeedAddress).ID);
                    if (!pk.Success)
                        return new ServiceResult<Address> { Success = false, Code = -1, Message = "PK failed" };

                    pks.Add(pk.Object);
                }

                var addressResponse = _blockchainServ.GenerateMultisigAddress(pks, request.MinSigsRequired, _configServ.Network);
                if (!addressResponse.Success)
                    return new ServiceResult<Address> { Success = false, Code = -1, Message = addressResponse.Message };

                var found = this.AddressExist(addressResponse.Object.Address);
                if (found != null)
                    return new ServiceResult<Address> { Success = true, Code = 2, Message = "Address already exists", Object = found };

                newAddress = new Infrastructure.Models.Address
                {
                    Label = request.Label,
                    Description = request.Description,
                    CurrencyID = request.CurrencyID,
                    KeyNumber = 0,
                    Addr = addressResponse.Object.Address,
                    IsMulti = true
                };
            }

            var id = _addressRepo.Insert(newAddress).Result;
            if (id == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address insert failed" };

            //Add lookup
            foreach (var userWalletID in request.UserWalletIDs)
            {
                var resultWalletAddress = _walletAddressServ.Create(new WalletAddressCreateRequest
                {
                    UserWalletID = userWalletID,
                    AddressID = id
                });
                if (!resultWalletAddress.Success)
                    return new ServiceResult<Address> { Success = false, Code = -1, Message = "Wallet address insert failed" };
            }

            return new ServiceResult<Address> { Success = true, Code = 1, Message = "Address created", Object = _mapper.Map<Address>(_addressRepo.GetById(id).Result) };
        }
        public ServiceResult<Address> Update(AddressUpdateRequest request)
        {
            var found = _addressRepo.GetById(request.ID).Result;
            if (found == null)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address not found" };

            found.Balance = request.Balance;
            found.BalanceConfirmed = request.BalanceConfirmed;
            found.Fiat = request.Fiat;

            _addressRepo.Update(found);

            return new ServiceResult<Address> { Success = true, Code = 1, Message = "Address updated", Object = _mapper.Map<Address>(_addressRepo.GetById(request.ID).Result) };
        }
        public ServiceResult<Address> Balance(int addressId)
        {
            var found = _addressRepo.GetById(addressId).Result;
            if (found == null || found.ID == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address not found" };

            var result = _blockchainServ.Balance(found.Addr, true, _configServ.Network);
            if (!result.Success)
                return new ServiceResult<Address> { Success = false, Code = result.Code, Message = result.Message };

            found.Balance = result.Object.Balance;
            found.BalanceConfirmed = result.Object.BalanceConfirmed;
            var updateResult = this.Update(_mapper.Map<AddressUpdateRequest>(found));
            if (!updateResult.Success)
                return new ServiceResult<Address> { Success = false, Code = updateResult.Code, Message = updateResult.Message };

            return new ServiceResult<Address> { Success = true, Code = 1, Object = _mapper.Map<Address>(_addressRepo.GetById(addressId).Result) };
        }
        public ServiceResult<Address> Balance(string address)
        {
            var addr = this.Get(address);
            if (!addr.Success)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Address not found" };

            return this.Balance(addr.Object.ID);
        }
        private Address AddressExist(string addr)
        {
            var found = _addressRepo.Where(o => o.Addr == addr).Select(o => _mapper.Map<Address>(o)).FirstOrDefault();

            if (!string.IsNullOrEmpty(addr) && found == null)
                found = _mapper.Map<Address>(_addressRepo.Where(o => o.Addr.ToLower() == addr.ToLower()).FirstOrDefault());

            return found;
        }
        private Address SetAdditional(Address addr)
        {
            var walletAddresses = _walletAddressServ.GetByAddressID(addr.ID);
            if (!walletAddresses.Success)
                throw new Exception("Wallet addresses not found");

            try
            {
                addr.UserWalletIDs = walletAddresses.Object.Where(x => x.AddressID == addr.ID).Select(x => x.UserWalletID).ToList();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            addr.IsMulti = this.AddressIsMultisig(addr);
            addr.IsExchangeAddress = this.AddressIsExchange(addr);
            addr.IsSeedAddress = this.AddressIsSeed(addr);

            return addr;
        }
        private List<Address> Filter(List<Address> addrs, Type type)
        {
            if (type == Type.Seed)
                addrs = addrs.Where(o => o.IsSeedAddress).ToList();
            else if (type == Type.Exchange)
                addrs = addrs.Where(o => o.IsExchangeAddress).ToList();
            else if (type == Type.Multisig)
                addrs = addrs.Where(o => o.IsMulti).ToList();

            return addrs;
        }
        private bool AddressIsMultisig(Address addr)
        {
            if (addr.UserWalletIDs.Count > 1)
                return true;

            return false;
        }
        private bool AddressIsExchange(Address addr)
        {
            if (addr.IsMulti)
            {
                foreach (var userWalletId in addr.UserWalletIDs)
                {
                    var user = _userServ.GetByUserWalletID(userWalletId);
                    if (!user.Success)
                        throw new Exception("User not found");

                    if (user.Object.UserTypeID == 5)
                        return true;
                }
            }

            return false;
        }
        private bool AddressIsSeed(Address addr)
        {
            if (!addr.IsMulti)
            {
                return true;
            }

            return false;
        }
    }
}
