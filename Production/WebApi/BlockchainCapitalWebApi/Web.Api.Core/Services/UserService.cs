﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IConfigService _configServ;
        private readonly IUserRepository _userRepo;
        private readonly IUserWalletService _userWalletServ;
        private readonly IBlockchainService _blockchainServ;
        private readonly IAddressRepository _addressRepo;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public UserService(IConfigService configServ, IUserRepository userRepo, IUserWalletService userWalletServ, IBlockchainService blockchainServ, IAddressRepository addressRepo, IConfiguration configuration, IMapper mapper)
        {
            _configServ = configServ;
            _userRepo = userRepo;
            _userWalletServ = userWalletServ;
            _blockchainServ = blockchainServ;
            _addressRepo = addressRepo;
            _config = configuration;
            _mapper = mapper;
        }

        public ServiceResult<List<User>> GetAll()
        {
            var users = _userRepo.GetAll().Result.Select(o => _mapper.Map<User>(o)).ToList();

            if (users == null)
                return new ServiceResult<List<User>> { Success = false, Code = -1, Message = "Failed to find all users" };

            return new ServiceResult<List<User>> { Success = true, Code = 1, Object = users };
        }
        public ServiceResult<User> Get(int id)
        {
            var user = _userRepo.GetById(id).Result;

            if (user == null || user.ID == 0)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "Failed to find user" };

            return new ServiceResult<User> { Success = true, Code = 1, Object = _mapper.Map<User>(user) };
        }
        public ServiceResult<User> GetByUserWalletID(int userWalletId)
        {
            var userWallet = _userWalletServ.Get(userWalletId);
            if (!userWallet.Success)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "Failed to find user wallet" };

            var user = _userRepo.GetById(userWallet.Object.UserID).Result;
            if (user == null || user.ID == 0)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "Failed to find user" };

            return new ServiceResult<User> { Success = true, Code = 1, Object = _mapper.Map<User>(user) };
        }
        public ServiceResult<User> GetByAddressID(int addressId)
        {
            var userWallets = _userWalletServ.GetByAddressID(addressId, UserWalletService.AddressOwner.NonExchange);
            if (!userWallets.Success || userWallets.Object.Count != 1)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "User wallet not found" };
            var userWallet = userWallets.Object.FirstOrDefault();

            var user = _userRepo.GetById(userWallet.UserID).Result;
            if (user == null || user.ID == 0)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "Failed to find user" };

            return new ServiceResult<User> { Success = true, Code = 1, Object = _mapper.Map<User>(user) };
        }
        public ServiceResult<User> Create(UserCreateRequest request)
        {
            var found = this.UsernameExist(request.Username);

            if (found != null)
                return new ServiceResult<User> { Success = true, Code = 1, Message = "Already exists" };

            Infrastructure.Models.User newUser = new Infrastructure.Models.User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                EmailAddress = request.Username,
                Username = request.Username,
                Password = request.Password,
                PasswordConfirm = request.PasswordConfirm
            };

            newUser.SCode = Helper.GeneratePassword(10);
            newUser.Password = Helper.EncodePassword(request.Password, newUser.SCode);

            var id = _userRepo.Insert(newUser).Result;
            if (id == 0)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "User insert failed" };

            return new ServiceResult<User> { Success = true, Code = 1, Message = "Created", Object = _mapper.Map<User>(_userRepo.GetById(id).Result) };
        }

        public ServiceResult<User> Login(UserLoginRequest request)
        {
            var found = this.UsernameExist(request.Username);

            if (found == null || found.ID == 0)
                return new ServiceResult<User> { Success = false, Code = -1, Message = "User not found" };

            if (found.Password == Helper.EncodePassword(request.Password, found.SCode))
            {
                var claims = new[]
                {
                        new Claim(JwtRegisteredClaimNames.Sub, found.Username),
                        new Claim(JwtRegisteredClaimNames.Jti, found.ID.ToString()),
                    };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_config["Tokens:Issuer"],
                _config["Tokens:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

                found.Token = new JwtSecurityTokenHandler().WriteToken(token);

                return new ServiceResult<User> { Success = true, Code = 1, Object = found };
            }

            return new ServiceResult<User> { Success = false, Code = -1, Message = "Password do not match" };
        }
        public ServiceResult<string> PrivateKey(int addressId)
        {
            var userWallets = _userWalletServ.GetByAddressID(addressId, UserWalletService.AddressOwner.NonExchange);
            if (!userWallets.Success || userWallets.Object.Count != 1)
                return new ServiceResult<string> { Success = false, Code = -1, Message = "User wallet not found" };
            var userWallet = userWallets.Object.FirstOrDefault();

            var address = _addressRepo.GetById(addressId).Result;
            if (address == null || address.ID == 0)
                return new ServiceResult<string> { Success = false, Code = -1, Message = "Address not found" };

            var result = _blockchainServ.GetPrivateKey(userWallet.PassPhrase, address.KeyNumber, _configServ.Network);
            if (!result.Success)
                return new ServiceResult<string> { Success = false, Code = result.Code, Message = result.Message };

            return new ServiceResult<string> { Success = true, Code = 1, Object = result.Object };
        }

        private User UsernameExist(string username)
        {
            var found = _userRepo.Where(u => u.Username.ToLower() == username.ToLower()).FirstOrDefault();

            return _mapper.Map<User>(found);
        }
    }
}
