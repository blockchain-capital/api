﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using System.Linq;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Interfaces;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Web.Api.Core.Models.Dto;

namespace Web.Api.Core.Services
{
    public class ExchangeService : IExchangeService
    {
        private readonly IConfigService _configServ;
        private readonly IAddressService _addressServ;
        private readonly ITransactionService _transactionServ;

        public enum ExchangeType
        {
            Crypto = 1,
            Fiat = 2
        }
        public ExchangeService(IAddressService addressServ, ITransactionService transactionServ, IConfigService configServ)
        {
            _addressServ = addressServ;
            _transactionServ = transactionServ;
            _configServ = configServ;
        }

        public ServiceResult<decimal> Balance(int userId, ExchangeType type)
        {
            var addr = this.GetAddress(userId);
            if (!addr.Success)
                return new ServiceResult<decimal> { Success = false, Code = -1, Message = "Address not found" };

            if (type == ExchangeType.Crypto)
            {
                var balance = _addressServ.Balance(addr.Object.ID);
                if (!balance.Success)
                    return new ServiceResult<decimal> { Success = false, Code = balance.Code, Message = balance.Message };

                return new ServiceResult<decimal> { Success = true, Code = 1, Object = balance.Object.BalanceConfirmed };
            }
            else if (type == ExchangeType.Fiat)
            {
                return new ServiceResult<decimal> { Success = true, Code = 1, Object = addr.Object.Fiat };
            }

            return new ServiceResult<decimal> { Success = false, Code = -1, Message = "Invalid type" };
        }
        public ServiceResult<Address> GetAddress(int userId)
        {
            var addrs = _addressServ.GetByUserID(userId, AddressService.Type.Exchange);
            if (!addrs.Success || addrs.Object.Count == 0)
                return new ServiceResult<Address> { Success = false, Code = -1, Message = "Addresses not found" };

            return new ServiceResult<Address> { Success = true, Code = 1, Object = addrs.Object.FirstOrDefault() };
        }
        public ServiceResult<Transaction> Sell(int userId, decimal amountBTC)
        {
            //Get the user exchange address
            var addr = this.GetAddress(userId);
            if (!addr.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Address not found" };

            //Get the exchange seed address
            var exchangeAddrs = _addressServ.GetByUserID(_configServ.ExchangeUserID, AddressService.Type.Seed);
            if (!exchangeAddrs.Success || exchangeAddrs.Object.Count == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Exchange addresses not found" };

            //Pay from user to exchange
            var trx = _transactionServ.Create(new TransactionCreateRequest
            {
                Type = TransactionType.SellToExchange,
                AmountBTC = amountBTC,
                SenderAddressID = addr.Object.ID,
                RecipientAddressID = exchangeAddrs.Object.FirstOrDefault().ID
            });
            if (!trx.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = trx.Message };

            return new ServiceResult<Transaction> { Success = true, Code = 1, Object = trx.Object };
        }
        public ServiceResult<Transaction> Buy(int userId, decimal amountFiat)
        {
            //Get the user exchange address
            var addr = this.GetAddress(userId);
            if (!addr.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Address not found" };

            //Get the exchange seed address
            var exchangeAddrs = _addressServ.GetByUserID(_configServ.ExchangeUserID, AddressService.Type.Seed);
            if (!exchangeAddrs.Success || exchangeAddrs.Object.Count == 0)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = "Exchange addresses not found" };

            //Pay from exchange to user
            var trx = _transactionServ.Create(new TransactionCreateRequest
            {
                Type = TransactionType.BuyFromExchange,
                Amount = amountFiat,
                SenderAddressID = exchangeAddrs.Object.FirstOrDefault().ID,
                RecipientAddressID = addr.Object.ID
            });
            if (!trx.Success)
                return new ServiceResult<Transaction> { Success = false, Code = -1, Message = trx.Message };

            return new ServiceResult<Transaction> { Success = true, Code = 1, Object = trx.Object };
        }
    }
}
