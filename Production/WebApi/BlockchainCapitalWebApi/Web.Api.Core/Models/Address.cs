﻿using System;
using System.Collections.Generic;

namespace Web.Api.Core.Models
{
    public class Address : BaseEntity
    {
        public int CurrencyID { get; set; }
        public int UserProcessID { get; set; }
        public bool IsMulti { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Addr { get; set; }
        public int KeyNumber { get; set; }
        public decimal Fiat { get; set; }
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public DateTime? Checked { get; set; }

        public List<int> UserWalletIDs { get; set; }

        //public List<UserWallet> Wallets { get; set; }
        //public List<Transaction> Transactions { get; set; }
        //public Currency Currency { get; set; }
        //public BTC BTC { get; set; }
        public bool IsExchangeAddress { get; set; }
        public bool IsSeedAddress { get; set; }
    }

    public class BTC
    {
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public int Confirms { get; set; }
        public int ConfirmsNeeded { get; set; }
    }
}
