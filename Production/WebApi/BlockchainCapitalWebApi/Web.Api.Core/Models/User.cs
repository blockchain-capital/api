﻿namespace Web.Api.Core.Models
{
    public class User : BaseEntity
    {
        public int UserTypeID { get; set; }
        public int RegionID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SCode { get; set; }
        public string Token { get; set; }
    }
}
