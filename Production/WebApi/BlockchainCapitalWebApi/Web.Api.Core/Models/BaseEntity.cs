﻿using System;

namespace Web.Api.Core.Models
{
    public abstract class BaseEntity
    {
        public int ID { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime Added { get; set; }
        public bool IsActive { get; set; }
    }
}
