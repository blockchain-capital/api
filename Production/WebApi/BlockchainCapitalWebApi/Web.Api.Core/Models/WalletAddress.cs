﻿namespace Web.Api.Core.Models
{
    public class WalletAddress : BaseEntity
    {
        public int UserWalletID { get; set; }
        public int AddressID { get; set; }
        public int UserAddressID { get; set; }
    }
}
