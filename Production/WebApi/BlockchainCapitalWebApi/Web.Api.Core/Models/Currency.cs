﻿namespace Web.Api.Core.Models
{
    public class Currency : BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
        public bool IsFiat { get; set; }
        public bool IsCrypto { get; set; }
    }
}
