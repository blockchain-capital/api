﻿namespace Web.Api.Core.Models
{
    public class Option : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public decimal ValueNumeric { get; set; }
        public string ValueText { get; set; }
    }
}
