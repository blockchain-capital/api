﻿namespace Web.Api.Core.Models.Dto
{
    public class UserWalletGetRequest
    {
        public int ID { get; set; }
        public int UserID { get; set; }
    }
    public class UserWalletGetResponse
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string PassPhrase { get; set; }
        public string SCode { get; set; }
    }
}
