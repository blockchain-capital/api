﻿namespace Web.Api.Core.Models.Dto
{
    public class UserWalletCreateRequest
    {
        public int UserID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string PassPhrase { get; set; }
    }
    public class UserWalletCreateResponse
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
    }
}
