﻿namespace Web.Api.Core.Models.Dto
{
    public class OptionCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public decimal ValueNumeric { get; set; }
        public string ValueText { get; set; }
    }
    public class OptionCreateResponse
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public decimal ValueNumeric { get; set; }
        public string ValueText { get; set; }
    }
}
