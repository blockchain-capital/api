﻿namespace Web.Api.Core.Models.Dto
{
    public class OptionGetRequest
    {
        public int ID { get; set; }
        public string Code { get; set; }
    }
    public class OptionGetResponse
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public decimal ValueNumeric { get; set; }
        public string ValueText { get; set; }
    }
}
