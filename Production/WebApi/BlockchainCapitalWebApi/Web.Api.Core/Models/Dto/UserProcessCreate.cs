﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class UserProcessCreateRequest
    {
        public int ProcessID { get; set; }
        public int UserAddressID { get; set; }
        public int MRTAddressID { get; set; }
        public int LenderAddressID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal InitialAmount { get; set; }
    }
    public class UserProcessCreateResponse : UserProcessGetRequest
    {
    }
}
