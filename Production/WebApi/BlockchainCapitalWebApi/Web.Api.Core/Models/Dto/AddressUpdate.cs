﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class AddressUpdateRequest
    {
        public int ID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Fiat { get; set; }
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public bool IsMulti { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
    }
    public class AddressUpdateResponse : AddressGetResponse
    {
        
    }
}
