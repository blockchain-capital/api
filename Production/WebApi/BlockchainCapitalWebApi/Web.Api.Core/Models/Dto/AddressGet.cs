﻿using System;
using System.Collections.Generic;

namespace Web.Api.Core.Models.Dto
{
    public class AddressGetRequest
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int UserProcessID { get; set; }
        public bool IsMulti { get; set; }
        public string Addr { get; set; }
        public int KeyNumber { get; set; }
    }
    public class AddressGetResponse : BaseEntity
    {
        public int CurrencyID { get; set; }
        public bool IsMulti { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Addr { get; set; }
        public int KeyNumber { get; set; }
        public decimal Fiat { get; set; }
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public DateTime? Checked { get; set; }
        public bool IsExchangeAddress { get; set; }
        public bool IsSeedAddress { get; set; }

        //public List<Transaction> Transactions { get; set; }
        //public Currency Currency { get; set; }
        //public BTC BTC { get; set; }
    }
}
