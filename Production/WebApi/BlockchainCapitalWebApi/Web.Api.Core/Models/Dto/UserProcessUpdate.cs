﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class UserProcessUpdateRequest
    {
        public int ID { get; set; }
        public int StatusID { get; set; }
        public int UserAddressID { get; set; }
        public int MRTAddressID { get; set; }
        public int LenderAddressID { get; set; }
        public int ProgressIndex { get; set; }
        public string ProgressMessage { get; set; }
        public bool HasError { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
    }
    public class UserProcessUpdateResponse : UserProcessGetRequest
    {
    }
}
