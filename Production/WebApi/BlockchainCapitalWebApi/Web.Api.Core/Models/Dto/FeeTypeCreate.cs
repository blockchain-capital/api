﻿namespace Web.Api.Core.Models.Dto
{
    public class FeeTypeCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
        public bool IsMandatory { get; set; }
    }
    public class FeeTypeCreateResponse
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
        public bool IsMandatory { get; set; }

        //public Currency Currency { get; set; }
    }
}
