﻿namespace Web.Api.Core.Models.Dto
{
    public class UserLoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class UserLoginResponse
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
