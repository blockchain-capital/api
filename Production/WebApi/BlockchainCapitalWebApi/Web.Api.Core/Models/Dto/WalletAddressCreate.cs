﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class WalletAddressCreateRequest
    {
        public int UserWalletID { get; set; }
        public int AddressID { get; set; }
        public int UserAddressID { get; set; }
    }
    public class WalletAddressCreateResponse : BaseEntity
    {
        public int UserWalletID { get; set; }
        public int AddressID { get; set; }
        public int UserAddressID { get; set; }
    }
}
