﻿namespace Web.Api.Core.Models.Dto
{
    public class UserCreateRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
    }
    public class UserCreateResponse
    {
        public int ID { get; set; }
        public int UserTypeID { get; set; }
        public int RegionID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
    }
}
