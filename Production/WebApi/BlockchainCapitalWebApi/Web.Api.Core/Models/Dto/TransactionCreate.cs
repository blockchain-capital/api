﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class TransactionCreateRequest
    {
        public TransactionType Type { get; set; }
        public int UserProcessID { get; set; }
        public int SenderAddressID { get; set; }
        public int RecipientAddressID { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountBTC { get; set; }
        public string Description { get; set; }
    }
    public class TransactionCreateResponse : TransactionGetResponse
    {
    }
}
