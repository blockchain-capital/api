﻿using System;

namespace Web.Api.Core.Models.Dto
{
    public class WalletAddressGetRequest
    {
        public int UserWalletID { get; set; }
        public int AddressID { get; set; }
        public int UserAddressID { get; set; }
    }
    public class WalletAddressGetResponse : BaseEntity
    {
        public int UserWalletID { get; set; }
        public int AddressID { get; set; }
        public int UserAddressID { get; set; }
    }
}
