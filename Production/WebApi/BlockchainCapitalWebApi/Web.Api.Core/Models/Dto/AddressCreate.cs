﻿using System;
using System.Collections.Generic;

namespace Web.Api.Core.Models.Dto
{
    public class AddressCreateRequest
    {
        public List<int> UserWalletIDs { get; set; }
        public int MinSigsRequired { get; set; }
        public int UserID { get; set; }
        public int CurrencyID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public bool IsExchangeAddress { get; set; }
    }
    public class AddressCreateResponse : AddressGetResponse
    {
        
    }
}
