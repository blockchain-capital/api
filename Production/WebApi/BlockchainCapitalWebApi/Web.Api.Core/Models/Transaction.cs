﻿using System;

namespace Web.Api.Core.Models
{
    public enum TransactionType
    {
        NotSpecified = 0,
        SellToExchange = 1,
        BuyFromExchange = 2
    }
    public class Transaction : BaseEntity
    {
        public TransactionType TypeID { get; set; }
        public int StatusID { get; set; }
        public int TransactionID { get; set; }
        public int UserProcessID { get; set; }
        public int SenderAddressID { get; set; }
        public int RecipientAddressID { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountBTC { get; set; }
        public string TxID { get; set; }
        public int Confirmations { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public DateTime? Processed { get; set; }
        //public User Sender { get; set; }
        //public User Recipient { get; set; }
        //public TransactionStatus Status { get; set; }
    }
}
