﻿namespace Web.Api.Core.Models
{
    public class FeeType : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
        public bool IsMandatory { get; set; }

        //public Currency Currency { get; set; }
    }
}
