﻿namespace Web.Api.Core.Models
{
    public class UserProcess : BaseEntity
    {
        public int ProcessID { get; set; }
        public int StatusID { get; set; }
        public int UserAddressID { get; set; }
        public int MRTAddressID { get; set; }
        public int LenderAddressID { get; set; }
        public int ProgressIndex { get; set; }
        public string ProgressMessage { get; set; }
        public bool HasError { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal InitialAmount { get; set; }
        public bool IsComplete { get; set; }

        //public Process Process { get; set; }
        //public Currency Currency { get; set; }
        //public UserProcessStatus Status { get; set; }
        //public Address Address { get; set; }
    }
}
