﻿namespace Web.Api.Core.Models
{
    public class Process : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
