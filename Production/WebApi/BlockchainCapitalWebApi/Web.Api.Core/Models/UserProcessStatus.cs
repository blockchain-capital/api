﻿namespace Web.Api.Core.Models
{
    public class UserProcessStatus : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
