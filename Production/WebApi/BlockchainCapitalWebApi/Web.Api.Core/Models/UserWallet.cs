﻿namespace Web.Api.Core.Models
{
    public class UserWallet : BaseEntity
    {
        public int UserID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string PassPhrase { get; set; }
        public string SCode { get; set; }
    }
}
