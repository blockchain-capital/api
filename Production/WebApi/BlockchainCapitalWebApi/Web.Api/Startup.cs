﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Monitors;
using Web.Api.Core.Interfaces.Processes;
using Web.Api.Core.Services;
using Web.Api.Core.Services.Monitors;
using Web.Api.Core.Services.Processes;
using Web.Api.Infrastructure;
using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Repositories;

namespace BlockchainCapitalWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")));

            services.AddAutoMapper();

            //Infrastructure repos
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<ICurrencyRepository, CurrencyRepository>();
            services.AddScoped<IFeeTypeRepository, FeeTypeRepository>();
            services.AddScoped<IOptionRepository, OptionRepository>();
            services.AddScoped<IProcessRepository, ProcessRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<ITransactionStatusRepository, TransactionStatusRepository>();
            services.AddScoped<IUserProcessRepository, UserProcessRepository>();
            services.AddScoped<IUserProcessStatusRepository, UserProcessStatusRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserWalletRepository, UserWalletRepository>();
            services.AddScoped<IWalletAddressRepository, WalletAddressRepository>();

            //Core services
            services.AddScoped<IConfigService, ConfigService>();
            services.AddScoped<IOptionService, OptionService>();
            services.AddScoped<IFeeTypeService, FeeTypeService>();
            services.AddScoped<IExchangeService, ExchangeService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserWalletService, UserWalletService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IWalletAddressService, WalletAddressService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IUserProcessService, UserProcessService>();

            //Blockchain services
            services.AddScoped<IBlockchainService, BlockchainService>();

            //Processes
            services.AddScoped<ILockGainsService, LockGainsService>();

            //Monitors
            services.AddScoped<ITransactionMonitorService, TransactionMonitorService>();
            services.AddScoped<IUserProcessMonitorService, UserProcessMonitorService>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Blockchain Capital Web API", Version = "v1" });
                c.IncludeXmlComments(Path.Combine(System.AppContext.BaseDirectory, "Web.Api.xml"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(o =>
            {
                o.DocumentTitle = "Blockchain Capital Developer Documentation";
                o.RoutePrefix = "developer";
                //o.RouteTemplate = "developer/{documentName}/docs.json";
                o.SwaggerEndpoint("/swagger/v1/swagger.json", "Version 1");
                o.InjectStylesheet("/swagger-ui/custom.css");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            app.UseStaticFiles();
        }
    }
}
