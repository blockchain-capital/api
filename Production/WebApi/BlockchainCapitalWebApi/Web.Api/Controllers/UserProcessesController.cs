﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/users/{userId}/processes")]
    [ApiController]
    public class UserProcessesController : Controller
    {
        private readonly IUserProcessService _userProcessServ;
        private readonly IMapper _mapper;

        private int _userId = 0;

        public UserProcessesController(IUserProcessService userProcessServ, IMapper mapper)
        {
            _userProcessServ = userProcessServ;
            _mapper = mapper;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            object userId;
            if (RouteData.Values.TryGetValue("userId", out userId))
                _userId = int.Parse(userId.ToString());

            base.OnActionExecuting(context);
        }

        /// <summary>
        /// Gets all the user processes
        /// </summary>
        /// <response code="200">UserProcessGetResponse array</response>
        /// <response code="404">User process not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserProcessGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _userProcessServ.GetAll(_userId);
            if (!result.Success)
                return NotFound();
            
            return Ok(result.Object.Select(o => _mapper.Map<UserProcessGetResponse>(o)));
        }

        /// <summary>
        /// Gets the user process that matches the id
        /// </summary>
        /// <param name="request">User process wallet id</param>
        /// <response code="200">UserProcessGetResponse object</response>
        /// <response code="404">User process not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserProcessGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var userProcessResult = _userProcessServ.Get(id);

            if (!userProcessResult.Success)
                return NotFound(userProcessResult.Message);

            var userProcess = _mapper.Map<UserProcessGetResponse>(userProcessResult.Object);

            if (userProcess == null || userProcess.ID == 0)
                return NotFound();

            return Ok(userProcess);
        }

        /// <summary>
        /// Creates a new user process
        /// </summary>
        /// <param name="request">UserProcessCreateRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">User process already exists</response>
        /// <response code="201">New user process created</response>
        /// <response code="400">Validation error</response>
        [HttpPost]
        [ProducesResponseType(typeof(UserProcessCreateResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] UserProcessCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _userProcessServ.Create(request);

            if (!result.Success)
                return BadRequest(result.Message);

            var userProcess = _mapper.Map<UserProcessCreateResponse>(result.Object);
            if (result.Code == 1)
                return Ok(userProcess);

            return CreatedAtAction("Get", new { id = userProcess.ID }, userProcess);
        }
    }
}
