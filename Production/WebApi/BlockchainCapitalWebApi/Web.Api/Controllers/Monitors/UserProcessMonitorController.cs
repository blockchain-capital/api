﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;
using Web.Api.Core.Interfaces.Monitors;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/monitors/users/processes")]
    [ApiController]
    public class UserProcessMonitorController : Controller
    {
        private readonly IUserProcessMonitorService _userProcessMonitorServ;
        private readonly IMapper _mapper;

        public UserProcessMonitorController(IUserProcessMonitorService userProcessMonitorServ, IMapper mapper)
        {
            _userProcessMonitorServ = userProcessMonitorServ;
            _mapper = mapper;
        }

        /// <summary>
        /// Run the monitor on all the user processes
        /// </summary>
        /// <response code="200">UserProcessGetResponse array</response>
        /// <response code="404">User processes not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserProcessGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _userProcessMonitorServ.Monitor();
            if (!result.Success)
                return NotFound(result.Message);
            
            return Ok(result.Object.Select(o => _mapper.Map<UserProcessGetResponse>(o)));
        }

        /// <summary>
        /// Runs the monitor on the user process id
        /// </summary>
        /// <param name="request">User process id</param>
        /// <response code="200">UserProcessGetResponse object</response>
        /// <response code="404">User process not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserProcessGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = _userProcessMonitorServ.Monitor(id);

            if (!result.Success)
                return NotFound(result.Message);

            var userProcess = _mapper.Map<UserProcessGetResponse>(result.Object);

            if (userProcess == null || userProcess.ID == 0)
                return NotFound();

            return Ok(userProcess);
        }
    }
}
