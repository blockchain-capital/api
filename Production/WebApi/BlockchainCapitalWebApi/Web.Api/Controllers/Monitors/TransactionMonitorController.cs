﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;
using Web.Api.Core.Interfaces.Monitors;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/monitors/transactions")]
    [ApiController]
    public class TransactionMonitorController : Controller
    {
        private readonly ITransactionMonitorService _transactionMonitorServ;
        private readonly IMapper _mapper;

        public TransactionMonitorController(ITransactionMonitorService transactionMonitorServ, IMapper mapper)
        {
            _transactionMonitorServ = transactionMonitorServ;
            _mapper = mapper;
        }

        /// <summary>
        /// Run the monitor on all the transactions
        /// </summary>
        /// <response code="200">TransactionGetResponse array</response>
        /// <response code="404">Transactions not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<TransactionGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _transactionMonitorServ.Monitor();
            if (!result.Success)
                return NotFound(result.Message);
            
            return Ok(result.Object.Select(o => _mapper.Map<TransactionGetResponse>(o)));
        }

        /// <summary>
        /// Runs the monitor on the transaction id
        /// </summary>
        /// <param name="request">Transaction id</param>
        /// <response code="200">TransactionGetResponse object</response>
        /// <response code="404">Transaction not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(TransactionGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = _transactionMonitorServ.Monitor(id);

            if (!result.Success)
                return NotFound(result.Message);

            var transaction = _mapper.Map<TransactionGetResponse>(result.Object);

            if (transaction == null || transaction.ID == 0)
                return NotFound();

            return Ok(transaction);
        }
    }
}
