﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/users/{userId}/wallets/{userWalletId}/addresses/{addressId}/transactions")]
    [ApiController]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionServ;
        private readonly IMapper _mapper;

        private int _userId = 0;
        private int _userWalletId = 0;
        private int _addressId = 0;

        public TransactionController(ITransactionService transactionServ, IMapper mapper)
        {
            _transactionServ = transactionServ;
            _mapper = mapper;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            object userId;
            object userWalletId;
            object addressId;
            if (RouteData.Values.TryGetValue("userId", out userId))
                _userId = int.Parse(userId.ToString());
            if (RouteData.Values.TryGetValue("userWalletId", out userWalletId))
                _userWalletId = int.Parse(userWalletId.ToString());
            if (RouteData.Values.TryGetValue("addressId", out addressId))
                _addressId = int.Parse(addressId.ToString());

            base.OnActionExecuting(context);
        }

        /// <summary>
        /// Gets all the transactions for the specified address id
        /// </summary>
        /// <response code="200">TransactionGetResponse array</response>
        /// <response code="404">Transactions not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<TransactionGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _transactionServ.GetAll(_addressId);
            if (!result.Success)
                return NotFound();
            
            return Ok(result.Object.Select(o => _mapper.Map<TransactionGetResponse>(o)));
        }

        /// <summary>
        /// Gets the transaction that matches the id
        /// </summary>
        /// <param name="request">Transaction id</param>
        /// <response code="200">TransactionGetResponse object</response>
        /// <response code="404">Transaction not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(TransactionGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var transactionResult = _transactionServ.Get(id);

            if (!transactionResult.Success)
                return NotFound(transactionResult.Message);

            var transaction = _mapper.Map<TransactionGetResponse>(transactionResult.Object);

            if (transaction == null || transaction.ID == 0)
                return NotFound();

            return Ok(transaction);
        }

        /// <summary>
        /// Creates a new transaction
        /// </summary>
        /// <param name="request">TransactionCreateRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">Transaction already exists</response>
        /// <response code="201">New transaction created</response>
        /// <response code="400">Validation error</response>
        [HttpPost]
        [ProducesResponseType(typeof(TransactionCreateResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] TransactionCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _transactionServ.Create(request);

            if (!result.Success)
                return BadRequest(result.Message);

            var transaction = _mapper.Map<TransactionCreateResponse>(result.Object);
            if (result.Code == 1)
                return Ok(transaction);

            return CreatedAtAction("Get", new { id = transaction.ID }, transaction);
        }
    }
}
