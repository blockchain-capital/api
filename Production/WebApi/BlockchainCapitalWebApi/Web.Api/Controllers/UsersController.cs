﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userServ;
        private readonly IMapper _mapper;

        public UsersController(IUserService userServ, IMapper mapper)
        {
            _userServ = userServ;
            _mapper = mapper;
        }

        //[HttpGet]
        //public IEnumerable<User> Get()
        //{
        //    return _userServ.GetAll();
        //}

        /// <summary>
        /// Gets the user that matches the id
        /// </summary>
        /// <param name="request">User id</param>
        /// <response code="200">UserGetResponse object</response>
        /// <response code="404">User not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var user = _mapper.Map<UserGetResponse>(_userServ.Get(id));

            if (user == null || user.ID == 0)
                return NotFound();

            return Ok(user);
        }

        /// <summary>
        /// Creates a new user with login credentials
        /// </summary>
        /// <param name="request">A UserCreateRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">User already exists</response>
        /// <response code="201">New user created</response>
        /// <response code="400">Passwords do not match</response>
        [HttpPost]
        [ProducesResponseType(typeof(UserCreateResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] UserCreateRequest request)
        {
            if (!ModelState.IsValid || request.Password != request.PasswordConfirm)
            {
                return BadRequest(ModelState);
            }

            var result = _userServ.Create(request);

            if (!result.Success)
                return BadRequest(result.Message);

            var user = _mapper.Map<UserCreateResponse>(result.Object);
            if (result.Code == 1)
                return Ok(user);

            return CreatedAtAction("Get", new { id = user.ID }, user);
        }

        /// <summary>
        /// Authenticates an existing user
        /// </summary>
        /// <param name="request">A UserLoginRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">User authenticated successfully (UserLoginResponse)</response>
        /// <response code="400">Failed to find or authenticate user (String)</response>
        [HttpPost("login")]
        [ProducesResponseType(typeof(UserLoginResponse), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = _userServ.Login(request);

            var user = _mapper.Map<UserLoginResponse>(result.Object);
            if (user != null && user.ID > 0)
            {
                if (!string.IsNullOrEmpty(user.Token))
                    return Ok(user);
                else
                    return BadRequest("Token not set");
            }

            return BadRequest("User not found");
        }

        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
