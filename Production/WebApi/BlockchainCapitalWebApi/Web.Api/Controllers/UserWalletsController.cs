﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/users/{userId}/wallets")]
    [ApiController]
    public class UserWalletsController : Controller
    {
        private readonly IUserWalletService _userWalletServ;
        private readonly IMapper _mapper;

        private int _userId = 0;

        public UserWalletsController(IUserWalletService userWalletServ, IMapper mapper)
        {
            _userWalletServ = userWalletServ;
            _mapper = mapper;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            object userId;
            if (RouteData.Values.TryGetValue("userId", out userId))
                _userId = int.Parse(userId.ToString());

            base.OnActionExecuting(context);
        }

        /// <summary>
        /// Gets all the user wallets
        /// </summary>
        /// <response code="200">UserWalletGetResponse array</response>
        /// <response code="404">User wallet not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserWalletGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _userWalletServ.GetAll(_userId);
            if (!result.Success)
                return NotFound();
            
            return Ok(result.Object.Select(o => _mapper.Map<UserWalletGetResponse>(o)));
        }

        /// <summary>
        /// Gets the user wallet that matches the id
        /// </summary>
        /// <param name="request">User wallet id</param>
        /// <response code="200">UserWalletGetResponse object</response>
        /// <response code="404">User wallet not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserWalletGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var userWalletResult = _userWalletServ.Get(id);

            if (!userWalletResult.Success)
                return NotFound(userWalletResult.Message);

            var userWallet = _mapper.Map<UserWalletGetResponse>(userWalletResult.Object);

            if (userWallet == null || userWallet.ID == 0)
                return NotFound();

            return Ok(userWallet);
        }

        /// <summary>
        /// Creates a new user wallet with a mnemonic
        /// </summary>
        /// <param name="request">A UserWalletCreateRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">User wallet already exists</response>
        /// <response code="201">New user wallet created</response>
        /// <response code="400">Validation error</response>
        [HttpPost]
        [ProducesResponseType(typeof(UserWalletCreateResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] UserWalletCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _userWalletServ.Create(request);

            if (!result.Success)
                return BadRequest(result.Message);

            var userWallet = _mapper.Map<UserWalletCreateResponse>(result.Object);
            if (result.Code == 1)
                return Ok(userWallet);

            return CreatedAtAction("Get", new { id = userWallet.ID }, userWallet);
        }
    }
}
