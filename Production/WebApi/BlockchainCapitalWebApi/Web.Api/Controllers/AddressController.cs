﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlockchainCapitalWebApi.Controllers
{
    [Route("api/users/{userId}/wallets/{userWalletId}/addresses")]
    [ApiController]
    public class AddressController : Controller
    {
        private readonly IAddressService _addressServ;
        private readonly IMapper _mapper;

        private int _userId = 0;
        private int _userWalletId = 0;

        public AddressController(IAddressService addressServ, IMapper mapper)
        {
            _addressServ = addressServ;
            _mapper = mapper;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            object userId;
            object userWalletId;
            if (RouteData.Values.TryGetValue("userId", out userId))
                _userId = int.Parse(userId.ToString());
            if (RouteData.Values.TryGetValue("userWalletId", out userWalletId))
                _userWalletId = int.Parse(userWalletId.ToString());

            base.OnActionExecuting(context);
        }

        /// <summary>
        /// Gets all the address
        /// </summary>
        /// <response code="200">AddressGetResponse array</response>
        /// <response code="404">Addresses not found</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<AddressGetResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            var result = _addressServ.GetByUserWalletID(_userWalletId);
            if (!result.Success)
                return NotFound();

            return Ok(result.Object.Select(o => _mapper.Map<AddressGetResponse>(o)));
        }

        /// <summary>
        /// Gets the address that matches the id
        /// </summary>
        /// <param name="request">Address id</param>
        /// <response code="200">AddressGetResponse object</response>
        /// <response code="404">Address not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AddressGetResponse), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = _addressServ.Get(id);
            if (!result.Success)
                return NotFound(result.Message);

            var address = _mapper.Map<AddressGetResponse>(result.Object);

            return Ok(address);
        }

        /// <summary>
        /// Creates a new address
        /// </summary>
        /// <param name="request">AddressCreateRequest model</param>
        /// <remarks></remarks>
        /// <response code="200">Address already exists</response>
        /// <response code="201">New address created</response>
        /// <response code="400">Validation issue</response>
        [HttpPost]
        [ProducesResponseType(typeof(AddressCreateResponse), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] AddressCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = _addressServ.Create(request);

            if (!result.Success)
                return BadRequest(result.Message);

            var address = _mapper.Map<AddressCreateResponse>(result.Object);
            if (result.Code == 1)
                return Ok(address);

            return CreatedAtAction("Get", new { id = address.ID }, address);
        }
    }
}
