﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class ProcessRepository : AbstractRepository<Process>, IProcessRepository
    {
        public ProcessRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
