﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class WalletAddressRepository : AbstractRepository<WalletAddress>, IWalletAddressRepository
    {
        public WalletAddressRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
