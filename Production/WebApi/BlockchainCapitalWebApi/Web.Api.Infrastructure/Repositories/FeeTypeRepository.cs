﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class FeeTypeRepository : AbstractRepository<FeeType>, IFeeTypeRepository
    {
        public FeeTypeRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
