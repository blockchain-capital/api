﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Web.Api.Infrastructure.Models;
using Web.Api.Infrastructure.Interfaces;

namespace Web.Api.Infrastructure.Repositories
{
    public class AbstractRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<T> _entities;

        public AbstractRepository(ApplicationDbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await _entities.ToListAsync();
        }
        public async Task<T> GetById(int id)
        {
            return await _entities.SingleOrDefaultAsync(s => s.ID == id);
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> exp)
        {
            return _entities.Where(exp);
        }
        public async Task<int> Insert(T entity)
        {
            await _entities.AddAsync(entity);
            _context.SaveChanges();

            return entity.ID;
        }
        public async void Update(T entity)
        {
            var oldEntity = await _context.FindAsync<T>(entity.ID);
            oldEntity.Updated = DateTime.Now;

            _context.Entry(oldEntity).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }
        public void Delete(T entity)
        {
            _entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
