﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class UserWalletRepository : AbstractRepository<UserWallet>, IUserWalletRepository
    {
        public UserWalletRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
