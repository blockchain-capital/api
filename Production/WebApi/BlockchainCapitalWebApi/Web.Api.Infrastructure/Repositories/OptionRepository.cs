﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class OptionRepository : AbstractRepository<Option>, IOptionRepository
    {
        public OptionRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
