﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class TransactionStatusRepository : AbstractRepository<TransactionStatus>, ITransactionStatusRepository
    {
        public TransactionStatusRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
