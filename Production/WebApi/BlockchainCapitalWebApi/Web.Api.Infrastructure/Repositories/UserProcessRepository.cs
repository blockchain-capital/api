﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class UserProcessRepository : AbstractRepository<UserProcess>, IUserProcessRepository
    {
        public UserProcessRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
