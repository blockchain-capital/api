﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class UserProcessStatusRepository : AbstractRepository<UserProcessStatus>, IUserProcessStatusRepository
    {
        public UserProcessStatusRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
