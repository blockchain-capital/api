﻿using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Repositories
{
    public class CurrencyRepository : AbstractRepository<Currency>, ICurrencyRepository
    {
        public CurrencyRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
