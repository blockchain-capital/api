﻿using Microsoft.EntityFrameworkCore;
using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<FeeType> FeeTypes { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<UserWallet> UserWallets { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<WalletAddress> WalletAddresses { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionStatus> TransactionStatuses { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<UserProcessStatus> UserProcessStatuses { get; set; }
        public DbSet<UserProcess> UserProcesses { get; set; }
    }
}
