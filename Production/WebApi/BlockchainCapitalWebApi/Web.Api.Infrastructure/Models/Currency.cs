﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("Currencies")]
    public class Currency : BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public bool IsFiat { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public bool IsCrypto { get; set; }
    }
}
