﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("Addresses")]
    public class Address : BaseEntity
    {
        public int CurrencyID { get; set; }
        public int UserProcessID { get; set; }
        public bool IsMulti { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Addr { get; set; }
        public int KeyNumber { get; set; }
        public decimal Fiat { get; set; }
        public decimal Balance { get; set; }
        public decimal BalanceConfirmed { get; set; }
        public DateTime? Checked { get; set; }
    }
}
