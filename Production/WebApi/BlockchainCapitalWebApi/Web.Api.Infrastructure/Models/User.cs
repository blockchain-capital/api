﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("Users")]
    public class User : BaseEntity
    {
        public int UserTypeID { get; set; }
        public int RegionID { get; set; }
        public int ExchangeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string SCode { get; set; }
        public DateTime? LastLogin { get; set; }

        [NotMapped]
        public string PasswordConfirm { get; set; }
        [NotMapped]
        public string Token { get; set; }
    }
}
