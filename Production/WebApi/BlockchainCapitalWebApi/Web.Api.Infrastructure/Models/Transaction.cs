﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("Transactions")]
    public class Transaction : BaseEntity
    {
        public int? TypeID { get; set; }
        public int? StatusID { get; set; }
        public int TransactionID { get; set; }
        public int UserProcessID { get; set; }
        public int SenderAddressID { get; set; }
        public int RecipientAddressID { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AmountBTC { get; set; }
        public string TxID { get; set; }
        public int Confirmations { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public DateTime? Processed { get; set; }
    }
}
