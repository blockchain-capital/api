﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("WalletAddresses")]
    public class WalletAddress : BaseEntity
    {
        public int? UserWalletID { get; set; }
        public int? AddressID { get; set; }
        public int? UserAddressID { get; set; }
    }
}
