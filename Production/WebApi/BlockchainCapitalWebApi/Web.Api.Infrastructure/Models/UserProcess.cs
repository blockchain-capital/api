﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("UserProcesses")]
    public class UserProcess : BaseEntity
    {
        public int ProcessID { get; set; }
        public int StatusID { get; set; }
        public int UserAddressID { get; set; }
        public int MRTAddressID { get; set; }
        public int LenderAddressID { get; set; }
        public int ProgressIndex { get; set; }
        public string ProgressMessage { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool HasError { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal InitialAmount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public bool IsComplete { get; set; }
    }
}
