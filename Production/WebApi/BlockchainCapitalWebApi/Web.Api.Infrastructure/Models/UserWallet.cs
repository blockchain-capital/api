﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Api.Infrastructure.Models
{
    [Table("UserWallets")]
    public class UserWallet : BaseEntity
    {
        public int UserID { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string PassPhrase { get; set; }
        public string SCode { get; set; }
    }
}
