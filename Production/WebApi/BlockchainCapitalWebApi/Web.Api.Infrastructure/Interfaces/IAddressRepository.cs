﻿using Web.Api.Infrastructure.Models;

namespace Web.Api.Infrastructure.Interfaces
{
    public interface IAddressRepository : IRepository<Address>
    {
    }
}
