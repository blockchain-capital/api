﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Models;
using Web.Api.Core.Models.Dto;
using Web.Api.Core.Services;
using Web.Api.Infrastructure.Interfaces;
using Xunit;
using static Web.Api.Core.Services.ExchangeService;

namespace Web.Api.Core.UnitTests.UseCases
{
    public class ExchangeUnitTests
    {
        Mock<IAddressService> mockAddressServ;
        Mock<ITransactionService> mockTransactionServ;
        Mock<IConfigService> mockConfigServ;
        ExchangeService useCase;

        public ExchangeUnitTests()
        {
            mockAddressServ = new Mock<IAddressService>();
            mockTransactionServ = new Mock<ITransactionService>();
            mockConfigServ = new Mock<IConfigService>();

            mockAddressServ
                .Setup(serv => serv.GetByUserID(It.IsAny<int>(), AddressService.Type.Seed))
                .Returns(new ServiceResult<List<Address>>
                {
                    Success = true,
                    Object = new List<Address>
                    {
                        new Address
                        {
                            ID = 1,
                            IsExchangeAddress = false,
                            Fiat = 0,
                            BalanceConfirmed = 0.02M
                        }
                    }
                });
            mockAddressServ
                .Setup(serv => serv.GetByUserID(It.IsAny<int>(), AddressService.Type.Exchange))
                .Returns(new ServiceResult<List<Address>>
                {
                    Success = true,
                    Object = new List<Address>
                    {
                        new Address
                        {
                            ID = 2,
                            IsExchangeAddress = true,
                            Fiat = 100,
                            BalanceConfirmed = 0.001M
                        }
                    }
                });
            mockTransactionServ
                .Setup(serv => serv.Create(It.IsAny<TransactionCreateRequest>()))
                .Returns(new ServiceResult<Transaction>
                {
                    Success = true
                });

            useCase = new ExchangeService(mockAddressServ.Object, mockTransactionServ.Object, mockConfigServ.Object);
        }

        [Fact]
        public async void Exchange_Address_Found_By_UserID()
        {
            // arrange
            
            // act
            var response = useCase.GetAddress(4);

            // assert
            Assert.True(response.Success && response.Object.ID != 0);
        }
        [Fact]
        public async void Exchange_Address_Balance_Check_Fiat()
        {
            // arrange

            // act
            var response = useCase.Balance(4, ExchangeType.Fiat);

            // assert
            Assert.True(response.Success && response.Object > 0);
        }
        [Fact]
        public async void Exchange_Address_Balance_Check_Crypto()
        {
            // arrange

            // act
            var response = useCase.Balance(4, ExchangeType.Crypto);

            // assert
            Assert.True(response.Success && response.Object == 0.001M);
        }
        [Fact]
        public async void Sell_Crypto_To_Exchange()
        {
            // arrange

            // act
            var response = useCase.Sell(4, 0.001M);

            // assert
            Assert.True(response.Success);
        }
        [Fact]
        public async void Buy_Crypto_From_Exchange()
        {
            // arrange

            // act
            var response = useCase.Buy(4, 100.00M);

            // assert
            Assert.True(response.Success);
        }
    }
}
